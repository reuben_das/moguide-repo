package com.sample.moguide.santa;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;
import com.sample.moguide.santa.helper.SimpleItemTouchHelperCallback;

import java.util.ArrayList;
import java.util.LinkedList;

public class RecyclerListFragment extends Fragment implements RecyclerListAdapter.OnDragStartListener{

    private ItemTouchHelper mItemTouchHelper;
    MainActivity mainReference;
    //public String[] STRINGS;

    public String[] STRING;
    LinkedList<String> desti;ArrayList<LatLng> sorti;ArrayList<Integer> icon;
    public RecyclerListFragment(MainActivity main, LinkedList<String> destination, ArrayList<LatLng> sorted,ArrayList<Integer> icons) {
        mainReference = main;
        desti=destination;
        sorti=sorted;
        icon=icons;
    }

    public RecyclerListFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //String[] STR = new String[]{"sdsdsd", "fhgdjsfdgjhks", "ff", "33", "44", "ss", "zz", "xx", "66", "88"};

        RecyclerListAdapter adapter = new RecyclerListAdapter(desti,sorti,mainReference,this,icon);
        //RecyclerListAdapter ad2 = new RecyclerListAdapter(desti,sorti,mainReference,this);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);
        //System.out.println("RECYCLER VIEW + " + mSend);
    }
    @Override
    public void onDragStarted(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
}
