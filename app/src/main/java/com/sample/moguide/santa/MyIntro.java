package com.sample.moguide.santa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro2;

public class MyIntro extends AppIntro2 {

    // Please DO NOT override onCreate. Use init.
    @Override
    public void init(Bundle savedInstanceState) {

        // Add your slide's fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.


        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest.
        addSlide(SampleSlide.newInstance(R.layout.swipe1));
        addSlide(SampleSlide.newInstance(R.layout.swipe2));
        addSlide(SampleSlide.newInstance(R.layout.swipe3));
        addSlide(SampleSlide.newInstance(R.layout.swipe4));
        addSlide(SampleSlide.newInstance(R.layout.swipe5));

        //askForPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        //askForPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},1);
        //askForPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        // OPTIONAL METHODS
        // Override bar/separator color.
        //setBarColor(Color.parseColor("#3F51B5"));
        //setSeparatorColor(Color.parseColor("#2196F3"));
        setFadeAnimation();

        // Hide Skip/Done button.
        //showSkipButton(false);
        setProgressButtonEnabled(true);;
    }

    private void loadMainActivity() {
        Intent intent = new Intent(this, Login2.class);
        intent.putExtra("Loaded",1);
        startActivity(intent);
    }



    @Override
    public void onDonePressed() {
        // Do something when users tap on Done button.
        loadMainActivity();
    }

    @Override
    public void onSlideChanged() {
        // Do something when the slide changes.
    }

    @Override
    public void onNextPressed() {
        // Do something when users tap on Next button.
    }

    public void getStarted(View v) {
        loadMainActivity();
    }

}
