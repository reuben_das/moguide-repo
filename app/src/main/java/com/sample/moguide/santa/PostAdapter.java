package com.sample.moguide.santa;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Reuben on 03/08/16.
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    public ArrayList<Post> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, name, review;
        public RatingBar ratingBar;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            name = (TextView) view.findViewById(R.id.name);
            review = (TextView) view.findViewById(R.id.review);
            ratingBar = (RatingBar) view.findViewById(R.id.bar);
        }
    }


    public PostAdapter(ArrayList<Post> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.postview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Post post = moviesList.get(position);
        holder.title.setText(post.getTitle());
        holder.name.setText(post.getAuthor());
        holder.review.setText(post.getBody());
        holder.ratingBar.setRating(post.getStarCount());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}





    /*public LayoutInflater inflater;
    public ArrayList<Post> posts;

    public class ViewHolder {
        TextView textView1;
        TextView textView2;
        TextView textView3;
        RatingBar ratingBar;
    }

    public PostAdapter(Context context,ArrayList<Post> posts){
        inflater = LayoutInflater.from(context);
        this.posts = posts;
    }


    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public Object getItem(int position) {
        return posts.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.postview, null);
            holder.textView1 = (TextView) convertView.findViewById(R.id.name);
            holder.textView2 = (TextView) convertView.findViewById(R.id.review);
            holder.textView3 = (TextView) convertView.findViewById(R.id.title);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.bar) ;
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textView1.setText(posts.get(position).getAuthor());
        holder.textView2.setText(posts.get(position).getBody());
        holder.textView3.setText(posts.get(position).getTitle());
        holder.ratingBar.setRating(posts.get(position).getStarCount());
        return convertView;
    }

    public void update(ArrayList<Post> posts){
        this.posts = posts;
        notifyDataSetChanged();
    }*/

