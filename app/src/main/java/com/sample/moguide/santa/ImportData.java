package com.sample.moguide.santa;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Reuben on 19/07/16.
 */

@IgnoreExtraProperties

public class ImportData {
    public Long rate1;
    public Long rate2;
    public Long rate3;
    public Long rate4;
    public Long rate5;
    public Long avg_rate;
    public String address;
    public String content;
    public String image_URL1;
    public String image_URL2;
    public Double lat;
    public Double lon;
    public Long number;
    public String places_to_visit;
    public String type;
    public String timing;
    public String key;

    public ImportData(String address, Double lat, Double lon, String places_to_visit) {
        this.address = address;
        this.lat = lat;
        this.lon = lon;
        this.places_to_visit = places_to_visit;
        this.type = "amenities";
    }

    public ImportData(String places_to_visit, String type) {
        this.places_to_visit = places_to_visit;
        this.type = type;
    }

    public ImportData() {

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getAvg_rate() {
        return avg_rate;
    }

    public Long getRate1() {
        return rate1;
    }

    public Long getRate2() {
        return rate2;
    }

    public Long getRate3() {
        return rate3;
    }

    public Long getRate4() {
        return rate4;
    }

    public Long getRate5() {
        return rate5;
    }

    public String getTiming() {
        return timing;
    }

    public String getAddress() {
        return address;
    }

    public String getContent() {
        return content;
    }

    public String getImage_URL1() {
        return image_URL1;
    }

    public String getImage_URL2() {
        return image_URL2;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public Long getNumber() {
        return number;
    }

    public String getPlaces_to_visit() {
        return places_to_visit;
    }

    public String getType() {
        return type;
    }

}
