package com.sample.moguide.santa;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.multidex.MultiDex;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.factual.driver.Factual;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;



public class MainActivity extends AppCompatActivity implements AbsListView.OnScrollListener, NavigationView.OnNavigationItemSelectedListener, OnShowcaseEventListener {

    public static final String TAG = "DemoActivity";
    public SlidingUpPanelLayout mLayout;

    public MaterialViewPager mViewPager;

    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;

    public int check_navigate = 0;
    //public LatLng curr_navi;
    //NotificationHandler nHandler;


    Button nav;

    public StaggeredGridLayoutManager _sGridLayoutManager;

    ShowcaseView sv;
    int sv_0 = 0;
    int sv_1 = 0;
    int sv_2 = 0;
    int sv_3 = 0;
    int sv_4 = 0;
    //int sv_5 = 0;
    int sv_6 = 0;
    ShowcaseView sv1;
    ShowcaseView sv2;
    ShowcaseView sv3;
    ShowcaseView sv4;
    //ShowcaseView sv5;
    ShowcaseView sv6;

    String key;
    //DataSnapshot dataSnapshot2;

    LocationManager lm;

    SwipeRefreshLayout mSwipeRefreshLayout;

    List<ItemObject> sList;

    RelativeLayout trendy;

    //ListView lv2;

    //ArrayList<FoursquareModel> venuesList;

    int jrch = 0;

    int counter = 0;


    LinearLayout l2;
    FrameLayout fl;
    RelativeLayout r2;

    //int itemflag = 0;
    int locations = 0;

    //int chk5 = 0;

    ArrayList<String> mapNameBase = new ArrayList<>();
    HashMap<String, Marker> mrkr = new HashMap<>();

    //final String CLIENT_ID = "SMMV3H1SNP031SZJUSLXK2A3YUNEFOL1QECZHLBDFXHTRZJE";
    //final String CLIENT_SECRET = "CO2JFEZEMFTFOA2VTTGPPSOSLBGRAYW5FL3XTX55JDHEB33U";

    String[] subcat = {"lodging", "null", "null", "null",
            "null", "null", "null", "null", "null", "null", "null", "null", "null",
            "null", "null", "null", "null",
            "shopping_mall", "shoe_store", "clothing_store", "home_goods_store",
            "amusement_park", "null", "null",
            "car_rental", "hospital", "pharmacy", "atm", "gas_station"};

    /*String[] subcat2 = {
            "4bf58dd8d48988d1f8931735",  //Bed & Breakfast
            "4bf58dd8d48988d1ee931735",  //Hostel
            "4bf58dd8d48988d12f951735",  //Resort
            "4f4530a74b9074f6e4fb0100",  //Boarding House
            "54135bf5e4b08f3d2429dfe1",  //Dhaba
            "54135bf5e4b08f3d2429dfdf",  //Indian Chinese Restaurant
            "4bf58dd8d48988d110941735",  //Italian Restaurant
            "54135bf5e4b08f3d2429dfe0",  //Multicuisine Indian Restaurant
            "54135bf5e4b08f3d2429dfde",  //South Indian Restaurant
            "54135bf5e4b08f3d2429dfdd",  //North Indian Restaurant
            "54135bf5e4b08f3d2429dff4",  //Mughlai Restaurant
            "54135bf5e4b08f3d2429dfea",  //Jain Restaurant
            "4bf58dd8d48988d16d941735",  //Café
            "4deefb944765f83613cdba6e",  //Historic Site
            "4bf58dd8d48988d182941735",  //Theme Park
            "4bf58dd8d48988d181941735",  //Museum
            "4bf58dd8d48988d131941735",  //Spiritual Center
            "4bf58dd8d48988d1fd941735",  //Shopping Mall
            "5744ccdfe4b0c0459246b4dc",  //Shopping Plaza
            "4bf58dd8d48988d1f7941735",  //Flea Market
            "4bf58dd8d48988d116951735",  //Antique Shop
            "4d4b7105d754a06377d81259",  //Outdoors & Recreation
            "4bf58dd8d48988d193941735",  //Water Park
            "4bf58dd8d48988d17b941735",  //Zoo
            "4e4c9077bd41f78e849722f9",  //Bike Rental
            "4bf58dd8d48988d196941735",  //Hospital
            "4bf58dd8d48988d10f951735",  //Pharmacy
            "52f2ab2ebcbc57f1066b8b56",  //ATM
            "4bf58dd8d48988d113951735"   //Gas Station
    };*/

    //private static final int MAX_ROWS = 3;
    private int lastTopValue = 0;

    //int first=0;

    //ViewPager pager;
    ViewPagerAdapter adapter;
    //SlidingTabLayout tabs;
    CharSequence Titles[]={"Description", "Address"};
    int Numboftabs =2;

    int start_flag = 0;
    int end_flag = 0;
    LatLng setLocation;
    Place F1;
    Place F2;

    public ImageView backgroundImage;

    Profile profile;
    View head;

    EditText strtl;
    EditText endl;

    NavigationView navigationView;
    NavigationView car;

    String pinName="N/A";
    public MapMe mapC;
    public RecyclerListFragment RLF;

    public int chu = 0;

    AdapterExp adapterExp;

    //List<String> modelList;

    ArrayList<Integer> listpos;
    ArrayList<Integer> childpos;
    //HashMap<String,Places> gmap;

    ProfilePictureView profileImage;
    TextView fbName;

    Button back3;

    public int sstatus = 0;

    Toolbar actionBar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout dleft;
    DrawerLayout d2;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ExpandableListView expandableListView;
    LinearLayout dragview;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    String fbid;
    View carHead;


    HashMap<String,List<String>> childmap = new HashMap<>();
    List<String> Headings = new ArrayList<>();


    int f1 = 0;
    int f2 = 0;
    int f3 = 0;
    int f4 = 0;
    int f5 = 0;
    int f6 = 0;

    int panheight=0;
    ViewPager viewPage;
    DatabaseReference ref;

    ArrayList<ImportData> mapdata;
    HashMap<String, ImportData> mapdata2;
    HashMap<String, ImportData> mapdata3;
    protected Factual factual = new Factual("M2Dke1WI4Y4V3RH4VQ6V4eEruWBLCI8wvRu6fM7W", "Ts9kxrVfjvhzxsshPWSwRQZqC8VuKiMBBeaMNJ1S");
    private TextView resultText = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        //FirebaseStorage storage = FirebaseStorage.getInstance();
        setContentView(R.layout.activity_main);
        FacebookSdk.sdkInitialize(this);
        mapC = new MapMe(this);
        //nHandler = NotificationHandler.getInstance(this);

        //listAnimation = new ListAnimation();

        //StorageReference storageRef = storage.getReferenceFromUrl("gs://applied-pipe-116817.appspot.com");
        //StorageReference ref = storageRef.child("FBid");

        //Firebase.setAndroidContext(this);
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        DatabaseReference myFirebaseRef = FirebaseDatabase.getInstance().getReference();
        //myFirebaseRef.child("message").setValue("Do you have data? You'll love Firebase.");

        /*myFirebaseRef.child("message").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Toast.makeText(getBaseContext(), snapshot.getValue().toString(), Toast.LENGTH_LONG).show();  //prints "Do you have data? You'll love Firebase."
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //myFirebaseRef.child("message2").setValue("Do you have data? You'll love Firebase. 2");
        myFirebaseRef.child("message2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //Toast.makeText(getBaseContext(),snapshot.getValue().toString(),Toast.LENGTH_LONG).show();  //prints "Do you have data? You'll love Firebase."
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            //Toast.makeText(getBaseContext(), "folo", Toast.LENGTH_LONG).show();
            DatabaseReference useref = myFirebaseRef.child("users/");
            DatabaseReference u = useref.child(user.getUid());
            u.child("email").setValue(user.getEmail());
            u.child("name").setValue(user.getDisplayName());
            u.child("uid").setValue(user.getUid());
            u.child("pid").setValue(user.getProviderData());
            u.child("providers").setValue(user.getProviders());
            //u.child("token").setValue(user.getToken().toString());

            // User is signed in
        } else {
            //Toast.makeText(getBaseContext(), "die", Toast.LENGTH_LONG).show();
            // No user is signed in
        }

        Intent intent = getIntent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        }
        String action = intent.getAction();
        final Uri data = intent.getData();

        mapdata = new ArrayList<>();
        mapdata2 = new HashMap<>();
        mapdata3 = new HashMap<>();


        if (data != null) {
            //Toast.makeText(this, data.getLastPathSegment(), Toast.LENGTH_LONG).show();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("moguide_test_data");
            ref.child(data.getLastPathSegment()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //Toast.makeText(getApplicationContext(),dataSnapshot.child("type").toString(),Toast.LENGTH_LONG).show();
                    ImportData importData = dataSnapshot.getValue(ImportData.class);
                    importData.setKey(dataSnapshot.getKey());
                    //System.out.println(dataSnapshot.getKey() + " was " + importData.getLat() + " meters tall");
                    //Toast.makeText(getApplicationContext(),dataSnapshot.getKey() + " was " + importData.getPlaces_to_visit() + " meters tall",Toast.LENGTH_SHORT).show();
                    mapC.googleMap.addMarker(new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ptv)));
                    mapC.savedMarkers.add(new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ptv)));
                    mapdata2.put(importData.getPlaces_to_visit(), importData);
                    mapC.baseMark.put(importData.getPlaces_to_visit(), new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ptv)));
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(importData.getLat(), importData.getLon())).zoom(10).build();
                    mapC.googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                    final String u1 = importData.getImage_URL1();
                    final String u2 = importData.getImage_URL2();

                    mViewPager.removeAllViews();

                    mViewPager.notifyHeaderChanged();
                    mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
                        @Override
                        public HeaderDesign getHeaderDesign(int page) {
                            switch (page) {
                                case 0:
                                    return HeaderDesign.fromColorResAndUrl(R.color.colorPrimary, u1);
                                case 1:
                                    if (u2 != null) {
                                        return HeaderDesign.fromColorResAndUrl(R.color.colorPrimary, u2);
                                    } else {
                                        return HeaderDesign.fromColorResAndUrl(R.color.colorPrimary, u1);
                                    }
                            }
                            //execute others actions if needed (ex : modify your header logo)
                            return null;
                        }
                    });
                    setPanelOn(importData.getPlaces_to_visit());
                    setWeatherAdapter(new LatLng(importData.getLat(), importData.getLon()), importData.getContent(), importData.getAddress(), "911", importData);
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }






        l2 = (LinearLayout) findViewById(R.id.fhleft2);
        fl = (FrameLayout)findViewById(R.id.fhleft);
        r2 = (RelativeLayout)findViewById(R.id.container_item);

        dragview = (LinearLayout) findViewById(R.id.dragView);

        trendy = (RelativeLayout) findViewById(R.id.trendy);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        actionBar=(Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(actionBar);
        //actionBar.setTitle("");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setIcon(R.drawable.name);

        dleft=(DrawerLayout)findViewById(R.id.drawerlayout);
        d2=(DrawerLayout)findViewById(R.id.d2);
        d2.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        car = (NavigationView) findViewById(R.id.car);
        carHead=car.getHeaderView(0);
        strtl =(EditText)carHead.findViewById(R.id.strtl);
        endl =(EditText)carHead.findViewById(R.id.endl);
        strtl.setInputType(InputType.TYPE_NULL);
        endl.setInputType(InputType.TYPE_NULL);
        strtl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jrch = 1;
                openAutocompleteActivity();
            }
        });

        endl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jrch = 2;
                openAutocompleteActivity();
            }
        });

        back3 = (Button)trendy.findViewById(R.id.back3);
        back3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trendy.setVisibility(View.INVISIBLE);
                DrawerLayout dleft = (DrawerLayout) findViewById(R.id.drawerlayout);
                dleft.closeDrawer(expandableListView);

            }
        });

        nav = (Button)r2.findViewById(R.id.navigate2);
        nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check_navigate==0)
                {
                    Toast.makeText(getApplicationContext(),"Select the Location to start Travelling",Toast.LENGTH_SHORT).show();
                    nav.setBackground(getResources().getDrawable(R.drawable.ic_close_white_48dp));
                    check_navigate=1;
                }
                else if(check_navigate==1)
                {
                    Toast.makeText(getApplicationContext(),"Navigation Cancelled",Toast.LENGTH_SHORT).show();
                    check_navigate=0;
                    nav.setBackground(getResources().getDrawable(R.drawable.ic_directions_white_48dp));
                }
            }
        });

        ImageButton backbt = (ImageButton)carHead.findViewById(R.id.backbt);
        backbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout d2 = (DrawerLayout)findViewById(R.id.d2);
                if(d2.isDrawerOpen(car)){
                    d2.closeDrawer(car);
                }
            }
        });
        Button end_jrny = (Button)carHead.findViewById(R.id.end_bt);
        end_jrny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((start_flag==3)&&(end_flag == 1))
                {
                    mapC.removeItem(setLocation);
                    mapC.removeItem(F2.getLatLng());
                    start_flag = 0;
                    end_flag = 0;
                }
                else if((start_flag==1)&&(end_flag == 1))
                {
                    mapC.removeItem(F1.getLatLng());
                    mapC.removeItem(F2.getLatLng());
                    start_flag = 0;
                    end_flag = 0;
                }
                strtl.setText("");
                endl.setText("");
            }
        });

        Button start_jrny = (Button)carHead.findViewById(R.id.start_bt);
        start_jrny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((start_flag==3)&&(end_flag == 1))
                {
                    mapC.addItem(setLocation);
                    mapC.flag2=1;
                    mapC.progressBar.setVisibility(View.VISIBLE);
                    mapC.progressBar.setIndeterminate(true);
                    mapC.addItem(F2.getLatLng(), 2, F2.getName().toString());
                    mapC.fab.setVisibility(View.VISIBLE);
                    locations++;
                    mapC.flag2=0;
                    start_flag = 0;
                    end_flag = 0;
                }
                else if((start_flag==1)&&(end_flag == 1))
                {
                    mapC.addItem(F1.getLatLng(), 1, F1.getName().toString());
                    mapC.flag2=1;
                    mapC.progressBar.setVisibility(View.VISIBLE);
                    mapC.progressBar.setIndeterminate(true);
                    mapC.addItem(F2.getLatLng(), 2, F2.getName().toString());
                    mapC.fab.setVisibility(View.VISIBLE);
                    locations++;
                    mapC.flag2=0;
                    start_flag = 0;
                    end_flag = 0;
                }
                DrawerLayout d2 = (DrawerLayout)findViewById(R.id.d2);
                if(d2.isDrawerOpen(car)){
                    d2.closeDrawer(car);
                }
                //SV();

            }
        });
        ImageButton locbt = (ImageButton)carHead.findViewById(R.id.loc_bt);
        locbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String ch = String.valueOf(mapC.mLastLocation.getLatitude());
                lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                boolean network_enabled = false;
                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                }

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch (Exception ex) {
                }

                if (!gps_enabled && !network_enabled) {
                    // notify user
                    AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                    dialog.setMessage("Enable your Location Services");
                    dialog.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            //get gps
                        }
                    });
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
                    dialog.show();
                } else {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mapC.setSearchResult(new LatLng(mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLongitude()));
                        setLocation = new LatLng(mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLongitude());
                        start_flag = 3;
                        //mapC.addItem(new LatLng(mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLongitude()));
                        strtl.setText("My Location");

                    }
                }

            }
        });

        profile = Profile.getCurrentProfile();

        fbid=profile.getId();


        head=navigationView.getHeaderView(0);
        fbName=(TextView)head.findViewById(R.id.name2);
        fbName.setText(profile.getFirstName()+" "+profile.getLastName());


        profileImage = (ProfilePictureView) head.findViewById(R.id.circleView);
        profileImage.setProfileId(profile.getId());

        listpos = new ArrayList<>();
        listpos.add(0);
        listpos.add(0);
        listpos.add(0);
        listpos.add(0);
        listpos.add(0);
        listpos.add(0);

        childpos = new ArrayList<>();
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);
        childpos.add(0);


        expandableListView = (ExpandableListView) findViewById(R.id.listright);
        //List<String> L = new ArrayList<>();
        List<String> L0 = new ArrayList<>();
        List<String> L1 = new ArrayList<>();
        List<String> L2 = new ArrayList<>();
        List<String> L3 = new ArrayList<>();
        List<String> L4 = new ArrayList<>();
        List<String> L5 = new ArrayList<>();
        List<String> L6 = new ArrayList<>();
        String heading_items[]=getResources().getStringArray(R.array.header_titles);

        //String l[]=getResources().getStringArray(R.array.blnk);
        String l0[]=getResources().getStringArray(R.array.blnk);
        String l1[] = getResources().getStringArray(R.array.blnk);
        String l2[] = getResources().getStringArray(R.array.Zomato);
        String l3[]=getResources().getStringArray(R.array.Sight);
        String l4[]=getResources().getStringArray(R.array.Shop);
        String l5[]=getResources().getStringArray(R.array.Ent);
        String l6[]=getResources().getStringArray(R.array.Amen);



        for(String title : heading_items){
            Headings.add(title);
        }

        /*for(String title : l){
            L.add(title);
        }*/

        for(String title : l0){
            L0.add(title);
        }

        for(String title : l1){
            L1.add(title);
        }

        for(String title : l2){
            L2.add(title);
        }

        for(String title : l3){
            L3.add(title);
        }

        for(String title : l4){
            L4.add(title);
        }

        for(String title : l5){
            L5.add(title);
        }

        for(String title : l6){
            L6.add(title);
        }


        //childmap.put(Headings.get(0),L);
        childmap.put(Headings.get(0), L0);
        childmap.put(Headings.get(1), L1);
        childmap.put(Headings.get(2), L2);
        childmap.put(Headings.get(3), L3);
        childmap.put(Headings.get(4), L4);
        childmap.put(Headings.get(5), L5);
        childmap.put(Headings.get(6), L6);


        adapterExp = new AdapterExp(this, Headings, childmap, listpos, childpos);
        expandableListView.setAdapter(adapterExp);

        actionBarDrawerToggle= new ActionBarDrawerToggle(this,dleft,actionBar,R.string.opendr,R.string.closedr);
        //dleft.addDrawerListener(actionBarDrawerToggle);
        dleft.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator();

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                /*if(groupPosition == 0)
                {
                    trendy.setVisibility(View.VISIBLE);
                }*/
                if (groupPosition == 0)
                {
                    buildWd();
                }

                if (groupPosition == 1) {
                    if (listpos.get(2) == 0) {
                        listpos.set(0, 1);
                        //childpos.set(0, 1);
                        /*if (mapC.mLastLocation != null) {
                            new GetPlaces(MainActivity.this, "lodging", 1, mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLongitude(), 2000).execute();
                        }*/
                    } else {
                        listpos.set(0, 0);
                        //childpos.set(0, 0);
                        /*for (int i = 0; i < mapNameBase.size(); i++) {
                            if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "lodging") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                mrkr.get(mapNameBase.get(i)).remove();
                                mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                            }
                        }*/
                    }

                }

                if (groupPosition == 2) {
                    if (listpos.get(1) == 0) {
                        listpos.set(1, 1);
                        //childpos.set(4, 1);
                        if (mapC.mLastLocation != null) {
                            //new GetPlaces(MainActivity.this, "restaurant", 1, mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLongitude(), 23000).execute();

                        }
                    } else {
                        listpos.set(1, 0);
                        //childpos.set(4, 0);
                        /*for (int i = 0; i < mapNameBase.size(); i++) {
                            if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "restaurant") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                mrkr.get(mapNameBase.get(i)).remove();
                                mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                            }
                        }*/
                    }
                }
                setFlags3(mapC.polyLineOptions, 1500, mapC.loc);
                adapterExp.updateAdapter(listpos);
                adapterExp.updateChild(childpos);
                return false;
            }
        });


        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                /*if (groupPosition == 1) {
                    if (childPosition == 0) {
                        if (childpos.get(0) == 0) {
                            childpos.set(0, 1);
                            //new GetPlaces(MainActivity.this, "lodging",0).execute();
                        } else {
                            childpos.set(0, 0);
                        }
                    }

                    if (childPosition == 1) {
                        if (childpos.get(1) == 0) {
                            childpos.set(1, 1);
                        } else {
                            childpos.set(1, 0);
                        }
                    }

                    if (childPosition == 2) {
                        if (childpos.get(2) == 0) {
                            childpos.set(2, 1);
                        } else {
                            childpos.set(2, 0);
                        }
                    }

                    if (childPosition == 3) {
                        if (childpos.get(3) == 0) {
                            childpos.set(3, 1);
                        } else {
                            childpos.set(3, 0);
                        }
                    }

                    listpos.set(0, childpos.get(0) + childpos.get(1) + childpos.get(2) + childpos.get(3));
                }*/

                if (groupPosition == 2) {
                    if (childPosition == 0) {
                        if (childpos.get(4) == 0) {
                            childpos.set(4, 1);
                            //new GetPlaces(MainActivity.this, "restaurant",1).execute();
                        } else {
                            childpos.set(4, 0);
                        }
                    }

                    if (childPosition == 1) {
                        if (childpos.get(5) == 0) {
                            childpos.set(5, 1);
                        } else {
                            childpos.set(5, 0);
                        }
                    }

                    if (childPosition == 2) {
                        if (childpos.get(6) == 0) {
                            childpos.set(6, 1);
                        } else {
                            childpos.set(6, 0);
                        }
                    }

                    if (childPosition == 3) {
                        if (childpos.get(7) == 0) {
                            childpos.set(7, 1);
                        } else {
                            childpos.set(7, 0);
                        }
                    }

                    if (childPosition == 4) {
                        if (childpos.get(8) == 0) {
                            childpos.set(8, 1);
                        } else {
                            childpos.set(8, 0);
                        }
                    }

                    listpos.set(1, childpos.get(4) + childpos.get(5) + childpos.get(6) + childpos.get(7) + childpos.get(8));
                }

                if (groupPosition == 3) {
                    if (childPosition == 0) {
                        if (childpos.get(13) == 0) {
                            childpos.set(13, 1);
                            //mapC.setMonument();

                        } else {
                            childpos.set(13, 0);
                        }
                    }

                    if (childPosition == 1) {
                        if (childpos.get(14) == 0) {

                            childpos.set(14, 1);
                        } else {
                            childpos.set(14, 0);

                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata2.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata2.get(mapNameBase.get(i)).getType(), "ptv")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapdata2.get(mapNameBase.get(i)).getPlaces_to_visit()).remove();
                                        mapdata2.remove(mapNameBase.get(i));
                                        mrkr.remove(mapNameBase.get(i));
                                        //Toast.makeText(getApplicationContext(),"Yo"+i,Toast.LENGTH_SHORT).show();
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                //Toast.makeText(getApplicationContext(),"yolo"+i,Toast.LENGTH_SHORT).show();
                                mapNameBase.remove(testlist.get(i));
                                //Toast.makeText(getApplicationContext(),""+mapNameBase.size()+mapNameBase.toString(),Toast.LENGTH_SHORT).show();
                                i++;
                            }


                        }
                    }

                    if (childPosition == 2) {
                        if (childpos.get(15) == 0) {

                            childpos.set(15, 1);
                        } else {
                            childpos.set(15, 0);
                            /*for (int i = 0; i < mapdata2.size(); i++) {
                                if (Objects.equals(mapdata2.get(mapNameBase.get(i)).getType(), "museum") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    Toast.makeText(getApplicationContext(),"onv"+i,Toast.LENGTH_SHORT).show();
                                }
                            }*/

                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata2.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata2.get(mapNameBase.get(i)).getType(), "museum")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapdata2.get(mapNameBase.get(i)).getPlaces_to_visit()).remove();
                                        mapdata2.remove(mapNameBase.get(i));
                                        mrkr.remove(mapNameBase.get(i));
                                        //Toast.makeText(getApplicationContext(),"Yo"+i,Toast.LENGTH_SHORT).show();
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                //Toast.makeText(getApplicationContext(),"yolo"+i,Toast.LENGTH_SHORT).show();
                                mapNameBase.remove(testlist.get(i));
                                //Toast.makeText(getApplicationContext(),""+mapNameBase.size()+mapNameBase.toString(),Toast.LENGTH_SHORT).show();
                                i++;
                            }
                        }
                    }

                    if (childPosition == 3) {
                        if (childpos.get(16) == 0) {
                            childpos.set(16, 1);
                        } else {
                            childpos.set(16, 0);
                            /*for (int i = 0; i < mapdata2.size(); i++) {
                                if (Objects.equals(mapdata2.get(mapNameBase.get(i)).getType(), "reli") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    Toast.makeText(getApplicationContext(),"ainv"+i,Toast.LENGTH_SHORT).show();
                                }
                            }*/
                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata2.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata2.get(mapNameBase.get(i)).getType(), "reli")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapdata2.get(mapNameBase.get(i)).getPlaces_to_visit()).remove();
                                        mapdata2.remove(mapNameBase.get(i));
                                        mrkr.remove(mapNameBase.get(i));
                                        //Toast.makeText(getApplicationContext(),"Yo"+i,Toast.LENGTH_SHORT).show();
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                //Toast.makeText(getApplicationContext(),"yolo"+i,Toast.LENGTH_SHORT).show();
                                mapNameBase.remove(testlist.get(i));
                                //Toast.makeText(getApplicationContext(),""+mapNameBase.size()+mapNameBase.toString(),Toast.LENGTH_SHORT).show();
                                i++;
                            }

                        }
                    }

                    if (childPosition == 4) {
                        if (childpos.get(9) == 0) {
                            childpos.set(9, 1);
                        } else {
                            childpos.set(9, 0);
                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata2.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata2.get(mapNameBase.get(i)).getType(), "park")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapdata2.get(mapNameBase.get(i)).getPlaces_to_visit()).remove();
                                        mapdata2.remove(mapNameBase.get(i));
                                        mrkr.remove(mapNameBase.get(i));
                                        //Toast.makeText(getApplicationContext(),"Yo"+i,Toast.LENGTH_SHORT).show();
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                //Toast.makeText(getApplicationContext(),"yolo"+i,Toast.LENGTH_SHORT).show();
                                mapNameBase.remove(testlist.get(i));
                                //Toast.makeText(getApplicationContext(),""+mapNameBase.size()+mapNameBase.toString(),Toast.LENGTH_SHORT).show();
                                i++;
                            }

                        }
                    }

                    listpos.set(2, childpos.get(13) + childpos.get(14) + childpos.get(15) + childpos.get(16) + childpos.get(9));
                }

                if (groupPosition == 4) {
                    if (childPosition == 0) {
                        if (childpos.get(17) == 0) {
                            //new GetPlaces(MainActivity.this, "shopping_mall",3).execute();
                            childpos.set(17, 1);
                        } else {
                            childpos.set(17, 0);
                            for (int i = 0; i < mapdata3.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "shopping_mall") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    if (childPosition == 1) {
                        if (childpos.get(18) == 0) {
                            // new GetPlaces(MainActivity.this, "shoe_store",3).execute();
                            childpos.set(18, 1);
                        } else {
                            childpos.set(18, 0);
                            for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "shoe_store") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    //mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    if (childPosition == 2) {
                        if (childpos.get(19) == 0) {
                            //new GetPlaces(MainActivity.this, "clothing_store",3).execute();
                            childpos.set(19, 1);
                        } else {
                            childpos.set(19, 0);
                            for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "clothing_store") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    if (childPosition == 3) {
                        if (childpos.get(20) == 0) {
                            //new GetPlaces(MainActivity.this, "home_goods_store",3).execute();
                            childpos.set(20, 1);
                        } else {
                            childpos.set(20, 0);
                            for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "home_goods_store") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                    listpos.set(3, childpos.get(17) + childpos.get(18) + childpos.get(19) + childpos.get(20));
                }

                if (groupPosition == 5) {
                    if (childPosition == 0) {
                        if (childpos.get(21) == 0) {
                            childpos.set(21, 1);
                            //new GetPlaces(MainActivity.this, "amusement_park",4).execute();
                        } else {
                            childpos.set(21, 0);
                            for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "amusement_park") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    if (childPosition == 1) {
                        if (childpos.get(22) == 0) {
                            childpos.set(22, 1);
                        } else {
                            childpos.set(22, 0);
                        }
                    }

                    if (childPosition == 2) {
                        if (childpos.get(23) == 0) {
                            childpos.set(23, 1);
                        } else {
                            childpos.set(23, 0);
                            //new GetPlaces(MainActivity.this, "zoo", 4, mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLongitude(), 10000).execute();
                        }
                    }
                    listpos.set(4, childpos.get(21) + childpos.get(22) + childpos.get(23));
                }

                if (groupPosition == 6) {
                    if (childPosition == 0) {
                        if (childpos.get(24) == 0) {
                            childpos.set(24, 1);
                            //new GetPlaces(MainActivity.this, "car_rental", 5, mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLongitude(), 10000).execute();
                        } else {
                            childpos.set(24, 0);
                            /*for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "car_rental") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }*/

                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata3.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "car_rental")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapNameBase.get(i)).remove();
                                        mapdata3.remove(mapNameBase.get(i));
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                mapNameBase.remove(testlist.get(i));
                                i++;
                            }

                        }
                    }

                    if (childPosition == 1) {
                        if (childpos.get(25) == 0) {
                            childpos.set(25, 1);
                        } else {
                            childpos.set(25, 0);
                            /*for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "hospital") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }*/

                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata3.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "hospital")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapNameBase.get(i)).remove();
                                        mapdata3.remove(mapNameBase.get(i));
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                mapNameBase.remove(testlist.get(i));
                                i++;
                            }


                        }
                    }

                    if (childPosition == 2) {
                        if (childpos.get(26) == 0) {
                            childpos.set(26, 1);
                        } else {
                            childpos.set(26, 0);
                            /*for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "pharmacy") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }*/

                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata3.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "pharmacy")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapNameBase.get(i)).remove();
                                        mapdata3.remove(mapNameBase.get(i));
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                mapNameBase.remove(testlist.get(i));
                                i++;
                            }

                        }
                    }

                    if (childPosition == 3) {
                        if (childpos.get(27) == 0) {
                            childpos.set(27, 1);

                        } else {
                            childpos.set(27, 0);
                            /*Toast.makeText(getApplicationContext(),mapdata3.get(mapNameBase.get(2)).getPlaces_to_visit()+"",Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),mapNameBase.get(2)+"",Toast.LENGTH_SHORT).show();

                            for (int i = 0; i < mapNameBase.size(); i++) {
                                if(mapdata3.containsKey(mapNameBase.get(i))){
                                    if(mapdata3.get(mapNameBase.get(i)).getType()=="atm")
                                    Toast.makeText(getApplicationContext(),"Jingalala",Toast.LENGTH_SHORT).show();
                                }
                            }*/
                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata3.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "atm")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapNameBase.get(i)).remove();
                                        mapdata3.remove(mapNameBase.get(i));
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                mapNameBase.remove(testlist.get(i));
                                i++;
                            }

                            /*for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "atm") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    //mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }*/

                        }
                    }

                    if (childPosition == 4) {
                        if (childpos.get(28) == 0) {
                            childpos.set(28, 1);
                        } else {
                            childpos.set(28, 0);
                            /*for (int i = 0; i < mapNameBase.size(); i++) {
                                if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "gas_station") && !mapC.untouch.containsValue(mapNameBase.get(i))) {
                                    mrkr.get(mapNameBase.get(i)).remove();
                                    mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                    mapNameBase.remove(i);
                                    //Toast.makeText(getApplicationContext(), "inv" + i, Toast.LENGTH_SHORT).show();
                                }
                            }*/

                            int i = 0;
                            ArrayList<String> testlist = new ArrayList<String>();
                            while (i < mapNameBase.size()) {
                                if (mapdata3.containsKey(mapNameBase.get(i))) {
                                    if (Objects.equals(mapdata3.get(mapNameBase.get(i)).getType(), "gas_station")) {
                                        testlist.add(mapNameBase.get(i));
                                        mrkr.get(mapNameBase.get(i)).remove();
                                        mapdata3.remove(mapNameBase.get(i));
                                        //mapC.baseMark.get(mapNameBase.get(i)).visible(false);
                                        //mapNameBase.remove(i);
                                    }
                                }
                                i++;
                            }

                            i = 0;
                            while (i < testlist.size()) {
                                mapNameBase.remove(testlist.get(i));
                                i++;
                            }
                        }
                    }
                    listpos.set(5, childpos.get(24) + childpos.get(25) + childpos.get(26) + childpos.get(27) + childpos.get(28));
                }


                adapterExp.updateAdapter(listpos);
                adapterExp.updateChild(childpos);
                setFlags3(mapC.polyLineOptions, 1500, mapC.loc);
                setFlags5(mapC.polyLineOptions, 1, mapC.loc);
                return false;
            }
        });



        mViewPager = (MaterialViewPager) findViewById(R.id.materialViewPager);

        /*mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
            @Override
            public HeaderDesign getHeaderDesign(int page) {
                switch (page) {
                    case 0:
                        return HeaderDesign.fromColorResAndDrawable(
                                R.color.colorPrimary,
                                getResources().getDrawable(R.drawable.ziro));
                    case 1:
                        return HeaderDesign.fromColorResAndDrawable(
                                R.color.colorPrimary,
                                getResources().getDrawable(R.drawable.tawang));
                }

                //execute others actions if needed (ex : modify your header logo)

                return null;
            }
        });*/

        adapter =  new ViewPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        viewPage = mViewPager.getViewPager();
        viewPage.setAdapter(adapter);
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout2);
        mLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelExpanded(View panel) {
                Log.i(TAG, "onPanelExpanded");

            }

            @Override
            public void onPanelCollapsed(View panel) {
                Log.i(TAG, "onPanelCollapsed");

            }

            @Override
            public void onPanelAnchored(View panel) {
                Log.i(TAG, "onPanelAnchored");
            }

            @Override
            public void onPanelHidden(View panel) {
                Log.i(TAG, "onPanelHidden");
            }
        });



        mLayout.setEnabled(true);
        TextView t = (TextView) findViewById(R.id.name);
        t.setText(pinName);
        Button f = (Button) findViewById(R.id.follow);
        Button d = (Button) findViewById(R.id.removeItem);
        Button n = (Button) findViewById(R.id.navigate);
        Button s = (Button) findViewById(R.id.share);
        f.setMovementMethod(LinkMovementMethod.getInstance());
        d.setMovementMethod(LinkMovementMethod.getInstance());
        n.setMovementMethod(LinkMovementMethod.getInstance());
        s.setMovementMethod(LinkMovementMethod.getInstance());

        s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Sharing...", Toast.LENGTH_LONG).show();
                //Application yo = getApplication();
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                if (mapC.sk != null) {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("moguide_test_data");
                    com.google.firebase.database.Query qry = ref.orderByChild("places_to_visit").equalTo(mapC.sk);
                    qry.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            //key = dataSnapshot.getKey();
                            key = "Check out this place on MoGuide \n" + "http://www.moguide.co/places/" + dataSnapshot.getKey();
                            View view = findViewById(R.id.materialViewPager).getRootView();
                            //final String[] key = new String[1];
                            view.setDrawingCacheEnabled(true);
                            view.buildDrawingCache();
                            Bitmap bm = screenShot(view);
                            File file = saveBitmap(bm, "mgdsend.png");
                            Log.i("chase", "filepath: " + file.getAbsolutePath());
                            Uri uri = Uri.fromFile(new File(file.getAbsolutePath()));
                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, key);
                            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                            shareIntent.setType("image/*");
                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(Intent.createChooser(shareIntent, "share via"));

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            //Toast.makeText(getApplicationContext(), "onchildchanged", Toast.LENGTH_LONG).show();
                            if (mapdata2.containsKey(dataSnapshot.child("places_to_visit").toString())) {
                                ImportData importData = dataSnapshot.getValue(ImportData.class);
                                importData.setKey(dataSnapshot.getKey());
                                mapdata2.remove(importData.getPlaces_to_visit());
                                mapdata2.put(importData.getPlaces_to_visit(), importData);
                            }

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }
        });

        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(locations>0)
                {

                    mapC.progressBar.setVisibility(View.VISIBLE);
                    mapC.progressBar.setIndeterminate(true);
                }
                locations++;
                mapC.addItem();
                /*if(sv_3!=1) {
                    sv3.hide();
                }*/
                //mapC.SV5();

            }
        });
        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(locations==1)
                {
                    mapC.fab.setVisibility(View.INVISIBLE);
                }
                else
                {
                    //mapC.progressBar.setVisibility(View.VISIBLE);
                    mapC.progressBar.setIndeterminate(true);
                }
                locations--;
                mapC.removeItem();

            }
        });
        n.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapC.fab.setVisibility(View.INVISIBLE);
                /*if(sv_4!=1) {
                    sv4.hide();
                }*/
                lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                boolean network_enabled = false;
                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                }

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch (Exception ex) {
                }

                if (!gps_enabled && !network_enabled) {
                    // notify user
                    AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                    dialog.setMessage("Enable your Location Services");
                    dialog.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            //get gps
                        }
                    });
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
                    dialog.show();
                } else {
                    mapC.navi();
                }

            }
        });

        mLayout.setOverlayed(true);

        panheight=mLayout.getPanelHeight();

        setPanelOff();
        String [] str = new String[5];
        for(int i = 0 ; i <5 ; i++)
        {
            str[i]="abc"+i;
        }

        //mapC.updtstr();
        LinkedList<String> waypoints=new LinkedList<String>();
        //waypoints.addAll(mapC.str);
        RLF = new RecyclerListFragment(this,mapC.realItinerary.destination,mapC.realItinerary.sorted,mapC.icons_mark);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view2);
        recyclerView.setHasFixedSize(true);

        /*_sGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(_sGridLayoutManager);*/

        //sList = getListItemData();

        /*SampleRecyclerViewAdapter rcAdapter = new SampleRecyclerViewAdapter(
                MainActivity.this, sList,this);
        recyclerView.setAdapter(rcAdapter);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.swipe1,
                R.color.swipe2,
                R.color.swipe3,
                R.color.swipe4,
                R.color.swipe5);*/

        LoadSelection(0);
        //nHandler.createExpandableNotification(this);

    }


    private Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private static File saveBitmap(Bitmap bm, String fileName) {
        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 90, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public void setMrkTrend(int position){
            ItemObject itm = sList.get(position);
            mapC.addTrend(itm.getName(),itm.getLocation());
            DrawerLayout dleft = (DrawerLayout) findViewById(R.id.drawerlayout);
            dleft.closeDrawer(expandableListView);

    }


    /*private List<ItemObject> getListItemData()
    {
        List<ItemObject> listViewItems = new ArrayList<ItemObject>();
        listViewItems.add(new ItemObject("Kurnool", "Seemandhra",getResources().getDrawable(R.drawable.kurnool),new LatLng(15.8118442,78.039802)));
        listViewItems.add(new ItemObject("Ziro", "Arunachal Pradesh",getResources().getDrawable(R.drawable.ziro),new LatLng(27.5594944,93.8285208)));
        listViewItems.add(new ItemObject("Tawang", "Arunachal Pradesh",getResources().getDrawable(R.drawable.tawang),new LatLng(27.5739351,91.8718024)));
        listViewItems.add(new ItemObject("Majuli", "Assam",getResources().getDrawable(R.drawable.majuli),new LatLng(27.0074877,94.1946745)));
        listViewItems.add(new ItemObject("Manas National Park", "Assam",getResources().getDrawable(R.drawable.manas),new LatLng(26.6408611,91.0950703)));
        listViewItems.add(new ItemObject("Champaner-Pavagadh", "Gujarat",getResources().getDrawable(R.drawable.champaneer),new LatLng(22.4844952,73.532331)));
        listViewItems.add(new ItemObject("Patan", "Gujarat",getResources().getDrawable(R.drawable.patan),new LatLng(23.8481837,72.1223259)));
        listViewItems.add(new ItemObject("Khajjar", "Himachal Pradesh",getResources().getDrawable(R.drawable.khajjar),new LatLng(32.550361,76.0630535)));
        listViewItems.add(new ItemObject("Shojha", "Himachal Pradesh",getResources().getDrawable(R.drawable.shojha),new LatLng(31.5675117,77.3710012)));
        listViewItems.add(new ItemObject("Kasol", "Himachal Pradesh",getResources().getDrawable(R.drawable.kasol),new LatLng( 32.0098591,77.3143047)));
        listViewItems.add(new ItemObject("Spiti", "Himachal Pradesh",getResources().getDrawable(R.drawable.spiti),new LatLng(32.2461365,78.034916)));
        listViewItems.add(new ItemObject("Tirthan Valley", "Himachal Pradesh",getResources().getDrawable(R.drawable.tirthan),new LatLng(31.6815608,77.5067532)));
        listViewItems.add(new ItemObject("Nubra Valley", "Jammu & Kashmir",getResources().getDrawable(R.drawable.nubra),new LatLng(34.6863141,77.567288)));
        listViewItems.add(new ItemObject("Hemis", "Jammu & Kashmir",getResources().getDrawable(R.drawable.hemis),new LatLng(33.913992,77.7090884)));
        listViewItems.add(new ItemObject("Gokarna", "Karnataka",getResources().getDrawable(R.drawable.gokarna),new LatLng(14.5383275,74.3241405)));
        listViewItems.add(new ItemObject("Halebeedu", "Karnataka",getResources().getDrawable(R.drawable.halebeedu),new LatLng(13.21309,75.9931797)));

        return listViewItems;
    }*/


    public SlidingUpPanelLayout.PanelState getStateSlide()
    {
        return(mLayout.getPanelState());
    }




    @SuppressWarnings("ResourceType")
    public void LoadSelection(int i){
        if(mLayout.getPanelState()==SlidingUpPanelLayout.PanelState.EXPANDED||mLayout.getPanelState()== SlidingUpPanelLayout.PanelState.COLLAPSED)
        {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

        }

        fragmentManager=getSupportFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();


        if(i==0)
        {
            //RLF = new RecyclerListFragment(this,mapC.realItinerary.destination);
            if(chu==0)
            {
                fragmentTransaction.add(R.id.fhleft,mapC);
                fragmentTransaction.add(R.id.fhleft2,RLF);
                chu++;
            }
            l2.setVisibility(View.GONE);
            r2.setVisibility(View.GONE);
            fl.setVisibility(View.VISIBLE);
            trendy.setVisibility(View.GONE);
            //mapC.resetcam();
            //mapC.bhenchod();

        }
        if(i==1)
        {

            fragmentTransaction.remove(RLF);
            RLF = new RecyclerListFragment(this,mapC.realItinerary.destination,mapC.realItinerary.sorted,mapC.icons_mark);
            fragmentTransaction.add(R.id.fhleft2,RLF);
            //RLF = new RecyclerListFragment(this,mapC.retn());
            //RLF = new RecyclerListFragment(this,mapC.realItinerary.destination);
            //fl.setVisibility(View.GONE);
            l2.setVisibility(View.VISIBLE);
            r2.setVisibility(View.VISIBLE);
            mapC.updatecam();
        }
        if(i==3){

        }

        fragmentTransaction.commit();
        dleft.closeDrawer(GravityCompat.START);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Enable your Location Services");
            dialog.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }

        //SV1();
        //mapC.SV5();
        //ActionItemTarget target = new ActionItemTarget(this, R.id.trip);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        DrawerLayout dleft = (DrawerLayout) findViewById(R.id.drawerlayout);
        //Toast.makeText(this,"Open",Toast.LENGTH_SHORT).show();

        switch (item.getItemId()) {
            case R.id.home:
                //Toast.makeText(this, "Open", Toast.LENGTH_SHORT).show();
                //onBackPressed();
                break;
            case R.id.homeAsUp:
                //Toast.makeText(this, "Open", Toast.LENGTH_SHORT).show();
                //onBackPressed();
                break;
            case android.R.id.home:
                //Toast.makeText(this, "Open", Toast.LENGTH_SHORT).show();
                //onBackPressed();
                break;
        }

        if (id == R.id.homeAsUp) {
            //Toast.makeText(this, "Open", Toast.LENGTH_SHORT).show();
            if (dleft.isDrawerOpen(expandableListView)) {
                //Toast.makeText(this,"Open",Toast.LENGTH_SHORT);
            } else if (dleft.isDrawerOpen(GravityCompat.START)) {
                dleft.closeDrawer(GravityCompat.START);
            }
            else{
                dleft.closeDrawer(expandableListView);
                dleft.openDrawer(GravityCompat.START);
            }
            /*if(sv_6!=1) {
                sv6.hide();
            }*/
        }
        if (id == R.id.action_search) {
            //Toast.makeText(this, "Open2", Toast.LENGTH_SHORT).show();
            /*if(sv_0!=0) {
                sv.hide();
            }*/
            dleft.closeDrawer(expandableListView);
            sstatus = 0;
            openAutocompleteActivity();
        }
        if(id==R.id.sex){
            /*if(sv_2>0) {
                sv2.hide();
            }*/
            if(dleft.isDrawerOpen(expandableListView)){
                dleft.closeDrawer(expandableListView);
            }
            else{
                dleft.closeDrawer(GravityCompat.START);
                dleft.openDrawer(expandableListView);
            }
        }
        if(id==R.id.trip) {
            if(d2.isDrawerOpen(car)){
                d2.closeDrawer(car);
            }
            else{
                /*if(sv_1==1) {
                    sv1.hide();
                }*/
                d2.openDrawer(car);

            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void setPanelOff()
    {

        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);


    }

    public void setPanelExpanded()
    {

        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);


    }

    public void setPanelOn(String S)
    {
        pinName = S;
        TextView t = (TextView) findViewById(R.id.name);
        t.setText(pinName);
        mLayout.setPanelHeight(panheight);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout d2 = (DrawerLayout)findViewById(R.id.d2);
        DrawerLayout dleft = (DrawerLayout) findViewById(R.id.drawerlayout);
        if (d2.isDrawerOpen(car)) {
            d2.closeDrawer(car);
        } else if (!(dleft.isDrawerOpen(GravityCompat.START)) && mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.HIDDEN) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you want to quit the app ?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }else if(mLayout.getPanelState()==SlidingUpPanelLayout.PanelState.EXPANDED)
        {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        }else if(mLayout.getPanelState()==SlidingUpPanelLayout.PanelState.COLLAPSED)
        {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            mapC.slide=0;
        }else if (dleft.isDrawerOpen(GravityCompat.START)) {
            dleft.closeDrawer(GravityCompat.START);
        }
        //LoadSelection(0);

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Rect rect = new Rect();
        backgroundImage.getLocalVisibleRect(rect);
        if (lastTopValue != rect.top) {
            lastTopValue = rect.top;
            backgroundImage.setY((float) (rect.top / 2.0));
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String title = item.getTitle().toString();
        if (id == R.id.home) {
            LoadSelection(0);
            //Toast.makeText(this,"fuck",Toast.LENGTH_SHORT).show();
            System.out.println("One");
        } else if (id == R.id.item) {
            if(mapC.realItinerary.nOfNds>=0)
            {
                LoadSelection(1);

            }else {
                Toast.makeText(this,"empty",Toast.LENGTH_SHORT).show();
            }
            //System.out.println("One");
        } else if (id == R.id.nav_slideshow) {
            if(mapC.realItinerary.nOfNds>=0)
            {
                lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                boolean network_enabled = false;
                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                }

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch (Exception ex) {
                }

                if (!gps_enabled && !network_enabled) {
                    // notify user
                    AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                    dialog.setMessage("Enable your Location Services");
                    dialog.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            //get gps
                        }
                    });
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
                    dialog.show();
                } else {
                    mapC.navi();
                }

            }else {
                Toast.makeText(this,"empty",Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_manage) {
            final AccessToken fbAccessToken = AccessToken.getCurrentAccessToken();
            if (fbAccessToken != null) {
                LoginManager.getInstance().logOut();
                FirebaseAuth.getInstance().signOut();
            }
            SharedPreferences getPrefs = PreferenceManager
                    .getDefaultSharedPreferences(getBaseContext());
            //  Make a new preferences editor
            SharedPreferences.Editor e = getPrefs.edit();

            //  Edit preference to make it false because we don't want this to run again
            e.putBoolean("firstStart", true);
            e.putBoolean("firstStartS", true);
            //  Apply changes
            e.apply();
            Toast.makeText(this,"Logged Out",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, Splash.class);
            startActivity(intent);

        } else if (id == R.id.pref) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);

        }

        int si = mapC.names.size();
        for(int i=0;i<si;i++)
        {
            if (Objects.equals(title, mapC.names.get(i))) {
                mapC.loadPolySaved(title);
                //Toast.makeText(this,title,Toast.LENGTH_SHORT).show();
            }
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void openAutocompleteActivity() {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e(TAG, message);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    public void buildWd() {
        int PLACE_PICKER_REQUEST = 1;
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place Selected: " + place.getName());
                //Toast.makeText(this,"Place Selected: " + place.getName(),Toast.LENGTH_SHORT).show();
                // Format the place's details and display them in the TextView.
                //place.getLatLng();
                // Display attributions if required.

                if(sstatus==0) {
                    mapC.setSearchResult(place);
                    //SV2();
                }
                if (jrch == 1) {
                    strtl.setText(place.getName());
                    mapC.setSearchResult(place, 1);
                    //Toast.makeText(this,place.getName(),Toast.LENGTH_SHORT).show();
                    mapC.sk = place.getName().toString();
                    //mapC.addItem(place.getLatLng());
                    F1=place;
                    start_flag=1;
                }
                if (jrch == 2) {
                    endl.setText(place.getName());
                    mapC.setSearchResult(place, 2);
                    mapC.sk2 = place.getName().toString();
                    //Toast.makeText(this,place.getName(),Toast.LENGTH_SHORT).show();
                    //mapC.addItem(place.getLatLng());
                    F2=place;
                    end_flag=1;
                }
                CharSequence attributions = place.getAttributions();

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e(TAG, "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.

            }
        }
    }


    /**
     * Helper method to format information about a place nicely.
     */
    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Log.e(TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }
    public boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }



    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
        if(counter==0){

            counter++;
        }

    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

    }

    public void SV3(){
        if(sv_3==0) {
            sv3 = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(new ViewTarget(R.id.follow, this)) //Here is where you supply the id of the action bar item you want to display
                    .setContentTitle("Add Location")
                    .setStyle(R.style.CustomShowcaseTheme)
                    .build();
            sv3.setButtonText("Hide");
            sv3.hideButton();
            sv_3++;
        }
    }

    public void SV4(){
        if(sv_4==0) {
            sv4 = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(new ViewTarget(R.id.navigate, this)) //Here is where you supply the id of the action bar item you want to display
                    .setContentTitle("Navigate")
                    .setStyle(R.style.CustomShowcaseTheme)
                    .build();
            sv4.setButtonText("Hide");
            sv4.hideButton();
            sv_4++;
        }
    }



    public void SV(){
        if(sv_0==0) {
            sv = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(new ToolbarActionItemTarget(actionBar, R.id.action_search)) //Here is where you supply the id of the action bar item you want to display
                    .setContentTitle("Search for Places")
                    .setStyle(R.style.CustomShowcaseTheme)
                    .build();
            sv.setButtonText("Hide");
            sv.hideButton();
            sv_0++;
        }
    }

    public void SV1(){
        if(sv_1==0) {
            sv1 = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(new ToolbarActionItemTarget(actionBar, R.id.trip)) //Here is where you supply the id of the action bar item you want to display
                    .setContentTitle("Start Journey")
                    .setContentText(Html.fromHtml("Click on the icon to define the <b> Start </b> & <b>End</b> points for your Journey"))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .build();
            sv1.setButtonText("Hide");
            sv1.hideButton();
            sv_1++;
        }
    }

    public void SV2(){
        if(sv_2==0) {
            sv2 = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(new ToolbarActionItemTarget(actionBar, R.id.sex)) //Here is where you supply the id of the action bar item you want to display
                    .setContentTitle("Choose From Filters")
                    .setStyle(R.style.CustomShowcaseTheme)
                    .build();
            sv2.setButtonText("Hide");
            sv2.hideButton();
            sv_2++;
        }
    }

    public void SV6(){
        if(sv_6==0) {
            try {
                ViewTarget nv = ViewTargets.navigationButtonViewTarget(actionBar);
                sv6 = new ShowcaseView.Builder(this)
                        .withMaterialShowcase()
                        .hideOnTouchOutside()
                        .setTarget(nv) //Here is where you supply the id of the action bar item you want to display
                        .setContentTitle("Open Navigation Drawer")
                        .setStyle(R.style.CustomShowcaseTheme)
                        .build();
                sv6.setButtonText("Hide");
                sv6.hideButton();
                sv_6++;
            } catch (ViewTargets.MissingViewException e) {
                e.printStackTrace();
            }

        }
    }

    /*public void setWeatherAdapter(LatLng w,String content,String address,String phone) {
        if(address==null){
            Numboftabs=1;
        }else{
            Numboftabs=2;
        }
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs, w,content,address,phone);
        ViewPager viewPage = mViewPager.getViewPager();
        viewPage.setAdapter(adapter);
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
    }*/

    public void setWeatherAdapter(LatLng w, String content, String address, String phone, ImportData a) {
        Numboftabs = 2;
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs, w, content, address, phone, a);
        ViewPager viewPage = mViewPager.getViewPager();
        viewPage.setAdapter(adapter);
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
    }


    class GetPlaces extends AsyncTask<Void, Void, ArrayList<Places>> {

        private ProgressBar dialog2;
        private ProgressDialog dialog;
        private Context context;
        private String places;
        private Integer des;
        private Double latitude = 28.5436056;
        private Double longitude = 77.2056723;
        private Integer distance = 2000;

        public GetPlaces(Context context, String places, Integer des, Double latitude, Double longitude) {
            this.context = context;
            this.places = places;
            this.des = des;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public GetPlaces(Context context, String places, Integer des, Double latitude, Double longitude, Integer distance) {
            this.context = context;
            this.places = places;
            this.des = des;
            this.latitude = latitude;
            this.longitude = longitude;
            this.distance = distance;

        }

        public GetPlaces(Context context, String places, Integer des) {
            this.context = context;
            this.places = places;
            this.des = des;
        }


        @Override
        protected void onPostExecute(ArrayList<Places> result) {
            super.onPostExecute(result);
            /*if (dialog.isShowing()) {
            }*/
            Marker mark;
            for (int i = 0; i < result.size(); i++) {
                if (!mapNameBase.contains(result.get(i).getName())) {
                    mark = mapC.googleMap.addMarker(new MarkerOptions()
                        .title(result.get(i).getName())
                        .position(
                                new LatLng(result.get(i).getLatitude(), result
                                        .get(i).getLongitude()))
                        .snippet(result.get(i).getVicinity()));
                //mapdata2.put(result.get(i).getName(), new ImportData(result.get(i).getVicinity(), result.get(i).getLatitude(), result.get(i).getLongitude(), result.get(i).getName()));


                switch (des) {
                    case 0:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.hotel));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "lodging"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.hotel)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.hotel));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());
                        break;

                    case 4:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.dhaba));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "restaurant"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.dhaba)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.dhaba));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 17:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.mall));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "shopping_mall"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.mall)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.mall));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 18:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.flea));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "shoe_store"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.flea)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.flea));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 19:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.flea));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "clothing_store"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.flea)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.flea));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 20:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.handicraft));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "home_goods_store"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.handicraft)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.handicraft));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 21:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.water));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "amusement_park"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.water)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.water));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 24:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.rent));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "car_rental"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.rent)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.rent));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 25:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.hospital));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "hospital"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.hospital)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.hospital));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 26:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.pharmacy));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "pharmacy"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.pharmacy)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.pharmacy));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 27:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.atm));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "atm"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.atm)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.atm));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;

                    case 28:
                        mark.setIcon(BitmapDescriptorFactory
                                .fromResource(R.drawable.gas));
                        mapdata3.put(result.get(i).getName(), new ImportData(result.get(i).getName(), "gas_station"));
                        mapC.baseMark.put(result.get(i).getName(), new MarkerOptions().title(result.get(i).getName()).position(new LatLng(result.get(i).getLatitude(), result.get(i).getLongitude())).icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.gas)));
                        mapC.baseIcon.put(result.get(i).getName(), BitmapDescriptorFactory
                                .fromResource(R.drawable.gas));
                        mrkr.put(result.get(i).getName(), mark);
                        mapNameBase.add(result.get(i).getName());

                        break;


                }
                }

            }
            mapC.progressBar2.setVisibility(View.GONE);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mapC.progressBar2.setVisibility(View.VISIBLE);
            mapC.progressBar2.setIndeterminate(true);
            /*dialog2 =

            dialog = new ProgressDialog(context);
            dialog.setCancelable(false);
            dialog.setMessage("Loading..");
            dialog.isIndeterminate();
            dialog.show();*/
        }


        @Override
        protected ArrayList<Places> doInBackground(Void... arg0) {
            PlacesService service = new PlacesService(
                    "AIzaSyB7M9T43gca5d_ssJP7rBG95qiTL1emNrA", distance);
            ArrayList<Places> findPlaces = service.findPlaces(latitude, // 28.632808
                    longitude, places); // 77.218276

            /*for (int i = 0; i < findPlaces.size(); i++) {

                Places placeDetail = findPlaces.get(i);
                //Log.e(TAG, "places : " + placeDetail.getName());
            }*/
            return findPlaces;
        }

    }

    /*private void loginToForsquare() {
        // Call the webbrowser with the Foursquare OAuth login URL
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(
                Uri.parse(
                        "https://foursquare.com/oauth2/authenticate" +
                                "?client_id=" +"SMMV3H1SNP031SZJUSLXK2A3YUNEFOL1QECZHLBDFXHTRZJE" +
                                "&response_type=code" +
                                "&redirect_uri=moguidetest-android-app://xyz"));
        startActivity(intent);
    }*/


    public void setFlags5(PolylineOptions line, Integer dist, LatLng loc) {

        if (line != null) {
            List<LatLng> points = line.getPoints();
            for (int i = points.size() - 1; i >= 0; i--) {
                if (i % 150 == 0) {
                    setFlags4(points.get(i).latitude, points.get(i).longitude);
                    //mainRefrence.new GetPlaces(mainRefrence, "atm",5,points.get(i).latitude,points.get(i).longitude,1500).execute();
                }
            }
        } else {
            if (loc != null) {
                setFlags4(loc.latitude, loc.longitude);
            } else {
                setFlags4(mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLatitude());
            }

        }

    }


    public void setFlags4(final Double lat, final Double lon) {
        DatabaseReference ref;
        com.google.firebase.database.Query queryRef;

        if (childpos.get(14) == 1) {
            //new GetPlaces(MainActivity.this, "park",2).execute();
            ref = FirebaseDatabase.getInstance().getReference("moguide_test_data");
            queryRef = ref.orderByChild("type").equalTo("ptv");
            //com.firebase.client.Query queryRef2 = ref.orderByChild("places_to_visit").equalTo("m");
            queryRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ImportData importData = dataSnapshot.getValue(ImportData.class);
                    importData.setKey(dataSnapshot.getKey());
                    if (!mapNameBase.contains(importData.getPlaces_to_visit())) {
                        //System.out.println(dataSnapshot.getKey() + " was " + importData.getLat() + " meters tall");
                        //Toast.makeText(getApplicationContext(),dataSnapshot.getKey() + " was " + importData.getPlaces_to_visit() + " meters tall",Toast.LENGTH_SHORT).show();
                        float[] res = new float[3];
                        Location.distanceBetween(mapC.loc.latitude, mapC.loc.longitude, importData.getLat(), importData.getLon(), res);
                        if (res[0] <= 8000) {
                            Marker mark = mapC.googleMap.addMarker(new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.ptv)));
                            mapC.baseMark.put(importData.getPlaces_to_visit(), new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.ptv)));
                            mrkr.put(importData.getPlaces_to_visit(), mark);
                            mapdata2.put(importData.getPlaces_to_visit(), importData);
                            mapNameBase.add(importData.getPlaces_to_visit());
                        }
                    }
                    //Toast.makeText(getApplicationContext(),mapdata2.get(importData.getPlaces_to_visit()).getPlaces_to_visit(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    //Toast.makeText(getApplicationContext(), "onchildchanged", Toast.LENGTH_LONG).show();
                    if (mapdata2.containsKey(dataSnapshot.child("places_to_visit").toString())) {
                        ImportData importData = dataSnapshot.getValue(ImportData.class);
                        importData.setKey(dataSnapshot.getKey());
                        mapdata2.remove(importData.getPlaces_to_visit());
                        mapdata2.put(importData.getPlaces_to_visit(), importData);
                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }


            });


        }

        if (childpos.get(15) == 1) {
            ref = FirebaseDatabase.getInstance().getReference("moguide_test_data");
            queryRef = ref.orderByChild("type").equalTo("museum");
            //com.firebase.client.Query queryRef2 = ref.orderByChild("places_to_visit").equalTo("m");
            queryRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ImportData importData = dataSnapshot.getValue(ImportData.class);
                    importData.setKey(dataSnapshot.getKey());
                    //System.out.println(dataSnapshot.getKey() + " was " + importData.getLat() + " meters tall");
                    //Toast.makeText(getApplicationContext(),dataSnapshot.getKey() + " was " + importData.getPlaces_to_visit() + " meters tall",Toast.LENGTH_SHORT).show();
                    if (!mapNameBase.contains(importData.getPlaces_to_visit())) {
                        float[] res = new float[3];
                        Location.distanceBetween(lat, lon, importData.getLat(), importData.getLon(), res);
                        if (res[0] <= 8000) {
                            Marker mark = mapC.googleMap.addMarker(new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.museum)));
                            mapC.baseMark.put(importData.getPlaces_to_visit(), new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.museum)));
                            mrkr.put(importData.getPlaces_to_visit(), mark);
                            mapdata2.put(importData.getPlaces_to_visit(), importData);
                            mapNameBase.add(importData.getPlaces_to_visit());
                        }
                    }
                    //Toast.makeText(getApplicationContext(),mapdata2.get(importData.getPlaces_to_visit()).getPlaces_to_visit(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    //Toast.makeText(getApplicationContext(), "onchildchanged", Toast.LENGTH_LONG).show();
                    if (mapdata2.containsKey(dataSnapshot.child("places_to_visit").toString())) {
                        ImportData importData = dataSnapshot.getValue(ImportData.class);
                        importData.setKey(dataSnapshot.getKey());
                        mapdata2.remove(importData.getPlaces_to_visit());
                        mapdata2.put(importData.getPlaces_to_visit(), importData);
                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });

        }


        if (childpos.get(16) == 1) {
            ref = FirebaseDatabase.getInstance().getReference("moguide_test_data");
            queryRef = ref.orderByChild("type").equalTo("reli");
            //com.firebase.client.Query queryRef2 = ref.orderByChild("places_to_visit").equalTo("m");
            queryRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ImportData importData = dataSnapshot.getValue(ImportData.class);
                    importData.setKey(dataSnapshot.getKey());
                    //System.out.println(dataSnapshot.getKey() + " was " + importData.getLat() + " meters tall");
                    //Toast.makeText(getApplicationContext(),dataSnapshot.getKey() + " was " + importData.getPlaces_to_visit() + " meters tall",Toast.LENGTH_SHORT).show();
                    if (!mapNameBase.contains(importData.getPlaces_to_visit())) {
                        float[] res = new float[3];
                        Location.distanceBetween(lat, lon, importData.getLat(), importData.getLon(), res);
                        if (res[0] <= 8000) {

                            Marker mark = mapC.googleMap.addMarker(new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.worship)));
                            mapC.baseMark.put(importData.getPlaces_to_visit(), new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.worship)));
                            mrkr.put(importData.getPlaces_to_visit(), mark);
                            mapdata2.put(importData.getPlaces_to_visit(), importData);
                            mapNameBase.add(importData.getPlaces_to_visit());
                        }
                    }
                    //Toast.makeText(getApplicationContext(),mapdata2.get(importData.getPlaces_to_visit()).getPlaces_to_visit(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    //Toast.makeText(getApplicationContext(), "onchildchanged", Toast.LENGTH_LONG).show();
                    if (mapdata2.containsKey(dataSnapshot.child("places_to_visit").toString())) {
                        ImportData importData = dataSnapshot.getValue(ImportData.class);
                        importData.setKey(dataSnapshot.getKey());
                        mapdata2.remove(importData.getPlaces_to_visit());
                        mapdata2.put(importData.getPlaces_to_visit(), importData);
                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });
        }

        if (childpos.get(9) == 1) {
            ref = FirebaseDatabase.getInstance().getReference("moguide_test_data");
            queryRef = ref.orderByChild("type").equalTo("park");
            //com.firebase.client.Query queryRef2 = ref.orderByChild("places_to_visit").equalTo("m");
            queryRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ImportData importData = dataSnapshot.getValue(ImportData.class);
                    importData.setKey(dataSnapshot.getKey());
                    //System.out.println(dataSnapshot.getKey() + " was " + importData.getLat() + " meters tall");
                    //Toast.makeText(getApplicationContext(),dataSnapshot.getKey() + " was " + importData.getPlaces_to_visit() + " meters tall",Toast.LENGTH_SHORT).show();
                    if (!mapNameBase.contains(importData.getPlaces_to_visit())) {
                        float[] res = new float[3];
                        Location.distanceBetween(lat, lon, importData.getLat(), importData.getLon(), res);
                        if (res[0] <= 8000) {

                            Marker mark = mapC.googleMap.addMarker(new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.water)));
                            mapC.baseMark.put(importData.getPlaces_to_visit(), new MarkerOptions().title(importData.getPlaces_to_visit()).position(new LatLng(importData.getLat(), importData.getLon())).icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.water)));
                            mrkr.put(importData.getPlaces_to_visit(), mark);
                            mapdata2.put(importData.getPlaces_to_visit(), importData);
                            mapNameBase.add(importData.getPlaces_to_visit());
                        }
                    }
                    //Toast.makeText(getApplicationContext(),mapdata2.get(importData.getPlaces_to_visit()).getPlaces_to_visit(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    //Toast.makeText(getApplicationContext(), "onchildchanged", Toast.LENGTH_LONG).show();
                    if (mapdata2.containsKey(dataSnapshot.child("park").toString())) {
                        ImportData importData = dataSnapshot.getValue(ImportData.class);
                        importData.setKey(dataSnapshot.getKey());
                        mapdata2.remove(importData.getPlaces_to_visit());
                        mapdata2.put(importData.getPlaces_to_visit(), importData);
                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });
        }

    }



    public void setFlags3(PolylineOptions line, Integer dist, LatLng loc) {
        if (line != null) {
            List<LatLng> points = line.getPoints();
            for (int i = 0; i < points.size(); i++) {
                if (points.size() < 500) {
                    if (i % 50 == 0) {
                        setFlags2(1500, points.get(i).latitude, points.get(i).longitude);
                        //mainRefrence.new GetPlaces(mainRefrence, "atm",5,points.get(i).latitude,points.get(i).longitude,1500).execute();
                    }
                } else if (points.size() < 3000) {
                    if (i % 100 == 0) {
                        setFlags2(1500, points.get(i).latitude, points.get(i).longitude);
                        //mainRefrence.new GetPlaces(mainRefrence, "atm",5,points.get(i).latitude,points.get(i).longitude,1500).execute();
                    }

                } else {
                    if (i % 180 == 0) {
                        setFlags2(1500, points.get(i).latitude, points.get(i).longitude);
                        //mainRefrence.new GetPlaces(mainRefrence, "atm",5,points.get(i).latitude,points.get(i).longitude,1500).execute();
                    }

                }

            }
        } else {
            if (loc != null) {
                setFlags2(dist, loc.latitude, loc.longitude);
            } else {
                setFlags2(dist, mapC.mLastLocation.getLatitude(), mapC.mLastLocation.getLatitude());
            }

        }
    }


    public void setFlags2(Integer dist, Double lat, Double lon) {
        for (int i = 0; i < 29; i++) {
            if (childpos.get(i) == 1 && !Objects.equals(subcat[i], "null")) {
                new GetPlaces(MainActivity.this, subcat[i], i, lat, lon, dist).execute();
            }

        }
    }

    /*private class foursquare extends AsyncTask<View, Void, String> {

        String temp;
        String id;
        Integer radius;
        Double lat;
        Double lon;
        Integer icon;

        public foursquare(String id, Integer radius, Double lat, Double lon, Integer icon) {
            this.id = id;
            this.radius = radius;
            this.lat = lat;
            this.lon = lon;
            this.icon = icon;
        }

        @Override
        protected String doInBackground(View... urls) {
            // make Call to the url
            *//*temp = makeCall("https://api.foursquare.com/v2/venues/search?client_id="
                    + CLIENT_ID
                    + "&client_secret="
                    + CLIENT_SECRET
                    + "&v=20130815&ll="
                    + String.valueOf(28.5436056)
                    + ","
                    + String.valueOf(77.2056723))
                    + "&intent=browse&radius=10000&categoryId=4bf58dd8d48988d1f8931735";*//*
            temp = makeCall("https://api.foursquare.com/v2/venues/search?ll=" +
                    String.valueOf(lat) +
                    "," +
                    String.valueOf(lon) +
                    "&v=20130815&intent=browse&radius=" +
                    String.valueOf(radius) +
                    "&categoryId=" +
                    id +
                    "&client_secret=" +
                    CLIENT_SECRET +
                    "&client_id=" +
                    CLIENT_ID +
                    "");
            Log.e("Link ---- > ", temp);
            return "";
        }

        @Override
        protected void onPreExecute() {
            // we can start a progress bar here
        }

        @Override
        protected void onPostExecute(String result) {
            if (temp == null) {
                // we have an error to the call
                // we can also stop the progress bar
            } else {
                // all things went right

                // parseFoursquare venues search result
                venuesList = parseFoursquare(temp);
                switch (icon) {
                    case 0:
                        for (FoursquareModel model : venuesList) {

                            // create marker
                            MarkerOptions marker = new MarkerOptions().position(
                                    new LatLng(model.getLatitude(), model
                                            .getLongtitude())).title(model.getName());

                            // Changing marker icon
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.hotel2));

                            // adding marker
                            mapC.googleMap.addMarker(marker);
                            mapC.baseMark.put(model.getName(),marker);
                            mapC.baseIcon.put(model.getName(),BitmapDescriptorFactory
                                    .fromResource(R.drawable.hotel2));

                        }
                        break;

                    case 1:
                        for (FoursquareModel model : venuesList) {

                            // create marker
                            MarkerOptions marker = new MarkerOptions().position(
                                    new LatLng(model.getLatitude(), model
                                            .getLongtitude())).title(model.getName());

                            // Changing marker icon
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.eat2));

                            // adding marker
                            mapC.baseIcon.put(model.getName(),BitmapDescriptorFactory
                                    .fromResource(R.drawable.eat2));
                            mapC.googleMap.addMarker(marker);
                            mapC.baseMark.put(model.getName(),marker);

                        }
                        break;

                    case 2:
                        for (FoursquareModel model : venuesList) {

                            // create marker
                            MarkerOptions marker = new MarkerOptions().position(
                                    new LatLng(model.getLatitude(), model
                                            .getLongtitude())).title(model.getName());

                            // Changing marker icon
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.monument));

                            // adding marker
                            mapC.baseIcon.put(model.getName(),BitmapDescriptorFactory
                                    .fromResource(R.drawable.monument));
                            mapC.googleMap.addMarker(marker);
                            mapC.baseMark.put(model.getName(),marker);

                        }
                        break;

                    case 3:
                        for (FoursquareModel model : venuesList) {

                            // create marker
                            MarkerOptions marker = new MarkerOptions().position(
                                    new LatLng(model.getLatitude(), model
                                            .getLongtitude())).title(model.getName());

                            // Changing marker icon
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.shop2));
                            mapC.baseIcon.put(model.getName(),BitmapDescriptorFactory
                                    .fromResource(R.drawable.shop2));

                            // adding marker
                            mapC.googleMap.addMarker(marker);
                            mapC.baseMark.put(model.getName(),marker);

                        }
                        break;

                    case 4:
                        for (FoursquareModel model : venuesList) {

                            // create marker
                            MarkerOptions marker = new MarkerOptions().position(
                                    new LatLng(model.getLatitude(), model
                                            .getLongtitude())).title(model.getName());

                            // Changing marker icon
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.sight2));
                            mapC.baseIcon.put(model.getName(),BitmapDescriptorFactory
                                    .fromResource(R.drawable.sight2));

                            // adding marker
                            mapC.googleMap.addMarker(marker);
                            mapC.baseMark.put(model.getName(),marker);

                        }
                        break;

                    case 5:
                        for (FoursquareModel model : venuesList) {

                            // create marker
                            MarkerOptions marker = new MarkerOptions().position(
                                    new LatLng(model.getLatitude(), model
                                            .getLongtitude())).title(model.getName());

                            // Changing marker icon
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.amen2));
                            mapC.baseIcon.put(model.getName(),BitmapDescriptorFactory
                                    .fromResource(R.drawable.amen2));

                            // adding marker
                            mapC.googleMap.addMarker(marker);
                            mapC.baseMark.put(model.getName(),marker);

                        }
                        break;
                }

            }
        }
    }*/

    /*public static String makeCall(String url) {

        // string buffers the url
        StringBuffer buffer_string = new StringBuffer(url);
        String replyString = "";

        // instanciate an HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        // instanciate an HttpGet
        HttpGet httpget = new HttpGet(buffer_string.toString());

        try {
            // get the responce of the httpclient execution of the url
            org.apache.http.HttpResponse response = httpclient.execute(httpget);
            InputStream is = response.getEntity().getContent();

            // buffer input stream the result
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }
            // the result as a string is ready for parsing
            replyString = new String(baf.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // trim the whitespaces
        return replyString.trim();
    }

    private static ArrayList<FoursquareModel> parseFoursquare(
            final String response) {

        ArrayList<FoursquareModel> temp = new ArrayList<FoursquareModel>();
        try {

            // make an jsonObject in order to parse the response
            JSONObject jsonObject = new JSONObject(response);

            // make an jsonObject in order to parse the response
            if (jsonObject.has("response")) {
                if (jsonObject.getJSONObject("response").has("venues")) {
                    JSONArray jsonArray = jsonObject.getJSONObject("response")
                            .getJSONArray("venues");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        FoursquareModel poi = new FoursquareModel();

                        try {
                            if (jsonArray.getJSONObject(i).has("name")) {
                                poi.setName(jsonArray.getJSONObject(i)
                                        .getString("name"));

                                // We will get only those locations which has
                                // address
                                if (jsonArray.getJSONObject(i).has("location")) {
                                    if (jsonArray.getJSONObject(i)
                                            .getJSONObject("location")
                                            .has("address")) {
                                        poi.setAddress(jsonArray
                                                .getJSONObject(i)
                                                .getJSONObject("location")
                                                .getString("address"));

                                        if (jsonArray.getJSONObject(i)
                                                .getJSONObject("location")
                                                .has("lat")) {
                                            poi.setLatitude(jsonArray
                                                    .getJSONObject(i)
                                                    .getJSONObject("location")
                                                    .getString("lat"));
                                        }
                                        if (jsonArray.getJSONObject(i)
                                                .getJSONObject("location")
                                                .has("lng")) {
                                            poi.setLongtitude(jsonArray
                                                    .getJSONObject(i)
                                                    .getJSONObject("location")
                                                    .getString("lng"));
                                        }

                                        if (jsonArray.getJSONObject(i)
                                                .getJSONObject("location")
                                                .has("city")) {
                                            poi.setCity(jsonArray
                                                    .getJSONObject(i)
                                                    .getJSONObject("location")
                                                    .getString("city"));
                                        }
                                        if (jsonArray.getJSONObject(i)
                                                .getJSONObject("location")
                                                .has("country")) {
                                            poi.setCountry(jsonArray
                                                    .getJSONObject(i)
                                                    .getJSONObject("location")
                                                    .getString("country"));
                                        }
                                        if (jsonArray.getJSONObject(i).has(
                                                "categories")) {
                                            if (jsonArray.getJSONObject(i)
                                                    .getJSONArray("categories")
                                                    .length() > 0) {
                                                if (jsonArray
                                                        .getJSONObject(i)
                                                        .getJSONArray(
                                                                "categories")
                                                        .getJSONObject(0)
                                                        .has("name")) {
                                                    poi.setCategory(jsonArray
                                                            .getJSONObject(i)
                                                            .getJSONArray(
                                                                    "categories")
                                                            .getJSONObject(0)
                                                            .getString("name"));
                                                }
                                                if (jsonArray
                                                        .getJSONObject(i)
                                                        .getJSONArray(
                                                                "categories")
                                                        .getJSONObject(0)
                                                        .has("id")) {
                                                    poi.setCategoryID(jsonArray
                                                            .getJSONObject(i)
                                                            .getJSONArray(
                                                                    "categories")
                                                            .getJSONObject(0)
                                                            .getString("id"));
                                                }
                                                if (jsonArray
                                                        .getJSONObject(i)
                                                        .getJSONArray(
                                                                "categories")
                                                        .getJSONObject(0)
                                                        .has("icon")) {

                                                    poi.setCategoryIcon(jsonArray
                                                            .getJSONObject(i)
                                                            .getJSONArray(
                                                                    "categories")
                                                            .getJSONObject(0)
                                                            .getJSONObject(
                                                                    "icon")
                                                            .getString("prefix")
                                                            + "bg_32.png");
                                                }
                                            }
                                        }
                                        temp.add(poi);

                                    }
                                }

                            }
                        } catch (Exception e) {

                        }

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;

    }*/

    public void setup() {
        MainActivity.this.recreate();
    }

}



