package com.sample.moguide.santa;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;

/**
 * Created by Reuben on 06/05/16.
 */
public class SampleViewHolders extends RecyclerView.ViewHolder implements
        View.OnClickListener
{
    public TextView bookName;
    public TextView authorName;
    public ImageView img;
    public MainActivity mainRefrence;

    public SampleViewHolders(View itemView,MainActivity main)
    {
        super(itemView);
        mainRefrence = main;
        itemView.setOnClickListener(this);
        bookName = (TextView) itemView.findViewById(R.id.BookName);
        authorName = (TextView) itemView.findViewById(R.id.AuthorName);
        img = (ImageView) itemView.findViewById(R.id.country_photo);
    }

    @Override
    public void onClick(View view)
    {
        if(mainRefrence!=null) {
            mainRefrence.setMrkTrend(getAdapterPosition());
        }
    }
}