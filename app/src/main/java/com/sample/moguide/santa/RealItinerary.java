package com.sample.moguide.santa;

import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class RealItinerary {

    public int nOfNds = -1;
    int i, j;
    LatLng t;
    String ctr = "0";
    private int numberOfNodes;
    private Stack<Integer> stack = new Stack<>();
    double lat, lon;
    double duration;
    String des,dura;
    public String [] RetName;
    LinkedList<String> destination = new LinkedList<>();
    LinkedList<Double> distancebw = new LinkedList<>();
    LinkedList<Double> journeyt = new LinkedList<>();
    LinkedList<Double> latitude = new LinkedList<>();
    LinkedList<Double> longitude = new LinkedList<>();
    public LinkedList<RealItinerary> init = new LinkedList<>();
    LinkedList<String> durationArray;
    public ArrayList<LatLng> sorted;
    public MapMe mapReference;
    double dist=0;
    double d;
    double distancematrix[][];
    private  final double avg_speed = 30.0;
    String distanceStr;


    public RealItinerary(MapMe main) {
        nOfNds = -1;
        mapReference = main;
        //destination = new LinkedList<>();
        ctr = "0";
        numberOfNodes = 0;
        lat = lon = 0.0;
        des = null;
        distancematrix=null;
        //LinkedList<RealItinerary> init=new LinkedList<RealItinerary>();
        sorted = new ArrayList<>();
        RetName = new String[10];

        destination = new LinkedList<>();
        distancebw = new LinkedList<>();
        journeyt = new LinkedList<>();
        latitude = new LinkedList<>();
        longitude = new LinkedList<>();
        init = new LinkedList<>();
    }

    public RealItinerary() {
        nOfNds = -1;
        //destination = new LinkedList<>();
        destination.add(0,"NULL");
        //ctr = "7";
        lat = lon = 0.0;
        des = null;

        sorted = new ArrayList<>();
    }





    private  double distance(double lat1, double lon1, double lat2, double lon2) {
        float[] results = new float[1];
        Location.distanceBetween(lat1, lon1, lat2, lon2, results);
        double dist = results[0];System.out.println("Distance " + dist);
        return (dist);
    }

    public void addDestination(String desti, double lati, double longi, LinkedList<RealItinerary> itin,double[][] distmat) {
        try{
            distancematrix=distmat;
        }catch (NullPointerException e)
        {
            System.out.println("NullPointerException in addDestination function");
        }

        itin.add(new RealItinerary());
        itin.getLast().des = desti;
        itin.getLast().lat = lati;
        itin.getLast().lon = longi;
        nOfNds++;
        destination.clear();
        latitude.clear();
        longitude.clear();
        System.out.println("Number of nodes(nOfNds)="+nOfNds+", (numberOfNodes)="+numberOfNodes);
        System.out.println("Number of nodes(nOfNds)="+nOfNds+", (numberOfNodes)="+numberOfNodes);
        System.out.println("Number of nodes(nOfNds)="+nOfNds+", (numberOfNodes)="+numberOfNodes);
        System.out.println("Number of nodes(nOfNds)="+nOfNds+", (numberOfNodes)="+numberOfNodes);
        if (nOfNds == 1) {

            destination.add(itin.getFirst().des);
            latitude.add(itin.getFirst().lat);
            longitude.add(itin.getFirst().lon);
            destination.add(desti);
            latitude.add(lati);
            longitude.add(longi);
            sorted.add(new LatLng(lati,longi));
            System.out.println("\nThe destinations are visited as follows-:");
            System.out.println("\n" + destination);
            double dist = distance(itin.getFirst().lat, itin.getFirst().lon, latitude.getLast(), longitude.getLast());
            System.out.println("\nThe distance between " + destination.getFirst() + " and " + destination.getLast() + " is " + dist + " km.");

            //Toast.makeText(, "added", Toast.LENGTH_SHORT).show();

        } else if (nOfNds == 0) {

            destination.add(desti);
            latitude.add(lati);
            longitude.add(longi);
            sorted.add(new LatLng(lati,longi));
            System.out.println("\nThe destinations are visited as follows-:");
            System.out.println("\n" + destination);


        }else {
            sorted.clear();
            makeAdjMatrix(itin, nOfNds);

        }


    }

    public  void makeAdjMatrix(LinkedList<RealItinerary> init, int n) {

        System.out.println("NOfNDS" + n);
        System.out.println("The adjacency matrix is ");
        for (i = 1; i <= n + 1; i++) {
            System.out.println();
            for (j = 1; j <= n + 1; j++) {
                try {
                    System.out.print(distancematrix[i][j] + " ");
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    System.out.println("NullPointerException occurred at index distancematrix["+i+"]["+j+"]");
                }
            }
        }

        System.out.println("\nThe destinations are visited as follows-:");
        tsp(distancematrix, init);
        System.out.println("\n" + destination);
    }

    public void tsp(double adjacencyMatrix[][], LinkedList<RealItinerary> itin) {
        numberOfNodes = adjacencyMatrix[0].length - 1;
        int[] visited = new int[numberOfNodes + 1];
        visited[1] = 1;
        stack.push(1);
        int element, dst = 0, i;
        double min;
        boolean minFlag = false;
        System.out.print(itin.get(0).des + "\t");
        destination.add(itin.get(0).des);
        latitude.add(itin.get(0).lat);
        longitude.add(itin.get(0).lon);
        sorted.add(new LatLng(itin.get(0).lat, itin.get(0).lon));
        distancebw.clear();
        while (!stack.isEmpty()) {
            element = stack.peek();
            i = 1;
            min = Double.MAX_VALUE;
            while (i <= numberOfNodes) {
                if ((adjacencyMatrix[element][i] > 0.0) && (visited[i] == 0) && (min > adjacencyMatrix[element][i])) {
                    min = adjacencyMatrix[element][i];
                    dst = i;
                    minFlag = true;
                }
                i++;
            }
            if (minFlag) {
                visited[dst] = 1;
                distancebw.add(min);
                stack.push(dst);
                destination.add(itin.get(dst - 1).des);
                latitude.add(itin.get(dst - 1).lat);
                longitude.add(itin.get(dst - 1).lon);
                sorted.add(new LatLng(itin.get(dst - 1).lat,itin.get(dst-1).lon));
                minFlag = false;
                continue;
            }
            stack.pop();
        }
    }



    public void removeDestination(String desti, double lati, double longi, LinkedList<RealItinerary> itin, double distmat[][]) {
        int b = 0;
        System.out.println(latitude + "\n" + longitude + "\n" + destination + "\n" + sorted);
        System.out.println("Number of nodes(nOfNds)="+nOfNds+", (numberOfNodes)="+numberOfNodes);
        System.out.println("Number of nodes(nOfNds)="+nOfNds+", (numberOfNodes)="+numberOfNodes);
        System.out.println("Number of nodes(nOfNds)="+nOfNds+", (numberOfNodes)="+numberOfNodes);
        System.out.println("Number of nodes(nOfNds)="+nOfNds+", (numberOfNodes)="+numberOfNodes);
        try{
            distancematrix=distmat;
        }catch (NullPointerException e)
        {
            System.out.println("NullPointerException in removeDestination function");
        }
        if ((nOfNds == -1) || (itin.isEmpty())) {
            System.out.println("\nItinerary is Empty");
            //sendSort();
            return;
        } else if (nOfNds == 0) {
            if ((itin.get(0).lat == lati) && (itin.get(0).lon == longi)) {
                itin.clear();
                longitude.clear();
                latitude.clear();
                sorted.clear();
                b++;
                nOfNds--;
                return;
            }

        } else if (nOfNds == 1) {

            if ((itin.get(0).lat == lati) && (itin.get(0).lon == longi)) {
                itin.remove(0);
                latitude.remove(0);
                longitude.remove(0);
                destination.remove(0);
                sorted.remove(0);nOfNds--;

            } else if ((itin.get(1).lat == lati) && (itin.get(1).lon == longi)) {
                itin.remove(1);
                nOfNds--;
                latitude.remove(1);
                longitude.remove(1);
                destination.remove(1);
                sorted.remove(1);

            } else {
                System.out.println("Given destination not found");

            }
            return;
        }
        else {
            for (i = 0; i <= nOfNds; i++) {


                if ((itin.get(i).lat == lati) && (itin.get(i).lon == longi)) {
                    itin.remove(i);
                    b++;
                    nOfNds--;
                    break;
                }
            }


            if (b == 0) {
                System.out.println("Destination not found");

            } else {
                destination.clear();
                latitude.clear();
                longitude.clear();
                sorted.clear();
                makeAdjMatrix(itin, nOfNds);
            }

        }
    }


    private  String getMapsApiDirectionsUrl(LatLng Origin, LatLng Destination) {


        String sensor = "sensor=false";
        String init = "https://maps.googleapis.com/maps/api/directions/json?";
        String origin = "origin=" + Origin.latitude+","+Origin.longitude + "&";
        String destination = "destination=" + Destination.latitude+","+ Destination.longitude + "&";
        String key = "key=" + "AIzaSyDtFIYxRQVgvtviW8e_77PpNnekYDXN4bk";
        String url = init + origin + destination + key;
        System.out.println(url);
        //url="https://maps.googleapis.com/maps/api/directions/json?origin=28.5436056,77.2056723&destination=28.6129167,77.227321&key=AIzaSyDtFIYxRQVgvtviW8e_77PpNnekYDXN4bk";
        return url;
    }

    public LinkedList<String> ReturnSex()
    {
        if(destination.get(0)=="NULL")
        {
            for(int i=0;i<1;i++)
            {
                RetName[i]="The Itiniery is Empty";
            }
        }
        else
        {
            for(int i=0;i<destination.size();i++)
            {
                if(Objects.equals(destination.get(i), "Hello Maps") || Objects.equals(destination.get(i), "NEW ADDED")) {
                    destination.set(i,RetName[i]="Waypoint "+i);
                }
                else{
                    RetName[i]=destination.get(i);
                }
            }
        }
        return(destination);
    }

}
