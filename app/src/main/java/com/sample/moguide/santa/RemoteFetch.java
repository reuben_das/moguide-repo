package com.sample.moguide.santa;

/**
 * Created by Reuben on 15/06/16.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class RemoteFetch {

    private static final String OPEN_WEATHER_MAP_API =
            "http://api.openweathermap.org/data/2.5/weather?";

    public static JSONObject getJSON(Context context, LatLng w) {
        try {
            String u = "http://api.openweathermap.org/data/2.5/weather?lat=" + w.latitude + "&lon=" + w.longitude + "&units=metric&appid=" + "9e9720bdc9401b4cbbf8339f279f9a81";
            URL url = new URL(u);
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();
            //String text = "http://api.openweathermap.org/data/2.5/weather?lat=32.01&lon=77.32&appid=9e9720bdc9401b4cbbf8339f279f9a81";

            //connection.addRequestProperty("x-api-key",
            //context.getString(R.string.open_weather_maps_app_id));

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuffer json = new StringBuffer(1024);
            String tmp = "";
            while ((tmp = reader.readLine()) != null)
                json.append(tmp).append("\n");
            reader.close();

            JSONObject data = new JSONObject(json.toString());

            // This value will be 404 if the request was not
            // successful
            if (data.getInt("cod") != 200) {
                return null;
            }

            return data;
        } catch (Exception e) {
            return null;
        }
    }
}