package com.sample.moguide.santa;

/**
 * Created by Reuben on 02/08/16.
 */

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Item {

    public String title;
    public Double lat;
    public Double lon;

    public Item() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Double getLon() {
        return lon;
    }

    public Double getLat() {
        return lat;
    }

    public String getTitle() {
        return title;
    }

    public Item(String title, Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
        this.title = title;

    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("title", title);
        result.put("lon", lon);
        result.put("lat", lat);
        return result;
    }

}