package com.sample.moguide.santa;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class RecyclerViewYoutube extends Fragment {

    ObservableScrollView mScrollView;
    private static final String API_KEY = "AIzaSyARAWTtNKwYvSKdYEtEIx7ejoG_uPZXmhs";
    private static String VIDEO_ID = "THA_5cqAfCQ";
    String content = "";

    public StaggeredGridLayoutManager _sGridLayoutManager;
    List<ItemObject> sList;
    LatLng w;
    Typeface weatherFont;

    TextView cityField;
    TextView updatedField;
    TextView detailsField;
    TextView currentTemperatureField;
    TextView weatherIcon;
    TextView rating;
    RatingBar rateb;
    TextView total;
    Handler handler;
    CardView back;
    ImageView wt;
    ProgressBar progressBar;
    RelativeLayout hide;
    TextView history;
    ImportData a;
    FitChart fitChart;
    RelativeLayout rate;
    RelativeLayout show;
    YouTubePlayerSupportFragment youTubePlayerFragment;
    FragmentTransaction transaction;

    public RecyclerViewYoutube() {

    }

    public RecyclerViewYoutube(LatLng wr, String content) {
        handler = new Handler();
        this.w = wr;
        this.content = content;
    }

    public RecyclerViewYoutube(LatLng wr, String content, ImportData a) {
        handler = new Handler();
        this.w = wr;
        this.content = content;
        this.a = a;
    }

    

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_youtube, container, false);
        /*youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_layout, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                if (!wasRestored) {
                    player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                    player.cueVideo(VIDEO_ID);
                    //player.pause();
                    //player.play();
                }
            }


            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
                // YouTube error
                String errorMessage = error.toString();
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                Log.d("errorMessage:", errorMessage);
            }
        });*/
        rateb = (RatingBar) rootview.findViewById(R.id.star);
        rating = (TextView) rootview.findViewById(R.id.rating);
        total = (TextView) rootview.findViewById(R.id.user);
        rate = (RelativeLayout) rootview.findViewById(R.id.rate);
        show = (RelativeLayout) rootview.findViewById(R.id.nosh);
        history = (TextView) rootview.findViewById(R.id.history);
        history.setText(content);
        hide = (RelativeLayout) rootview.findViewById(R.id.rlayout);
        back = (CardView) rootview.findViewById(R.id.weather_card);
        cityField = (TextView) rootview.findViewById(R.id.city_field);
        updatedField = (TextView) rootview.findViewById(R.id.updated_field);
        detailsField = (TextView) rootview.findViewById(R.id.details_field);
        currentTemperatureField = (TextView) rootview.findViewById(R.id.current_temperature_field);
        weatherIcon = (TextView) rootview.findViewById(R.id.weather_icon);
        wt = (ImageView) rootview.findViewById(R.id.background_w);
        progressBar = (ProgressBar) rootview.findViewById(R.id.pbar);
        weatherFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/weather.ttf");
        weatherIcon.setTypeface(weatherFont);
        updateWeatherData();
        hide.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        if (a != null) {

            DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference("moguide_test_data");
            com.google.firebase.database.Query qry = ref2.orderByChild("places_to_visit").equalTo(a.getPlaces_to_visit());
            qry.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ImportData a = dataSnapshot.getValue(ImportData.class);
                    long full = a.getRate1() + a.getRate2() + a.getRate3() + a.getRate4() + a.getRate5();
                    if (full != 0) {
                        show.setVisibility(View.GONE);
                        rate.setVisibility(View.VISIBLE);
                        //Resources resources = getResources();
                        Float avg = (float) ((a.getRate5() * 5f + a.getRate4() * 4f + a.getRate3() * 3f + a.getRate2() * 2f + a.getRate1()) / (float) full);
                        //Toast.makeText(getContext(), avg + "", Toast.LENGTH_LONG).show();
                        rating.setText(avg + " ");
                        total.setText(full + " ");
                        rateb.setRating(avg);
                    } else {
                        show.setVisibility(View.VISIBLE);
                        rate.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    ImportData a = dataSnapshot.getValue(ImportData.class);
                    Long full = a.getRate1() + a.getRate2() + a.getRate3() + a.getRate4() + a.getRate5();
                    if (full != 0) {
                        show.setVisibility(View.GONE);
                        rate.setVisibility(View.VISIBLE);
                        /*Resources resources = getResources();
                        Collection<FitChartValue> values = new ArrayList<>();
                        values.add(new FitChartValue((a.getRate5() / full) * 100f, resources.getColor(R.color.chart5)));
                        values.add(new FitChartValue((a.getRate5() / full) * 100f, resources.getColor(R.color.chart4)));
                        values.add(new FitChartValue((a.getRate5() / full) * 100f, resources.getColor(R.color.chart3)));
                        values.add(new FitChartValue((a.getRate5() / full) * 100f, resources.getColor(R.color.chart2)));
                        values.add(new FitChartValue((a.getRate5() / full) * 100f, resources.getColor(R.color.chart1)));
                        fitChart.setValues(values);*/
                        Float avg = (float) ((a.getRate5() * 5 + a.getRate4() * 4 + a.getRate3() * 3 + a.getRate2() * 2 + a.getRate1()) / full);
                        rating.setText(avg + " ");
                        total.setText(full + " ");
                        //Toast.makeText(getContext(), avg + "", Toast.LENGTH_LONG).show();
                    } else {
                        show.setVisibility(View.VISIBLE);
                        rate.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return rootview;
    }

    private void updateWeatherData() {
        new Thread() {
            public void run() {
                final JSONObject json = RemoteFetch.getJSON(getActivity(), w);
                if (json == null) {
                    handler.post(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(),
                                    getActivity().getString(R.string.place_not_found),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        public void run() {
                            renderWeather(json);
                        }
                    });
                }
            }
        }.start();
    }


    private void renderWeather(JSONObject json) {
        try {
            cityField.setText(json.getString("name").toUpperCase(Locale.US) + ", " + json.getJSONObject("sys").getString("country"));
            //Toast.makeText(getContext(),"YOLO",Toast.LENGTH_SHORT).show();

            JSONObject details = json.getJSONArray("weather").getJSONObject(0);
            JSONObject main = json.getJSONObject("main");
            detailsField.setText(
                    details.getString("description").toUpperCase(Locale.US) +
                            "\n" + "Humidity: " + main.getString("humidity") + "%" +
                            "\n" + "Pressure: " + main.getString("pressure") + " hPa");

            currentTemperatureField.setText(
                    String.format("%.2f", main.getDouble("temp")) + " ℃");

            DateFormat df = DateFormat.getDateTimeInstance();
            String updatedOn = df.format(new Date(json.getLong("dt") * 1000));
            updatedField.setText("Last update: " + updatedOn);

            setWeatherIcon(details.getInt("id"),
                    json.getJSONObject("sys").getLong("sunrise") * 1000,
                    json.getJSONObject("sys").getLong("sunset") * 1000);

        } catch (Exception e) {
            Log.e("SimpleWeather", "One or more fields not found in the JSON data");
        }
    }

    private void setWeatherIcon(int actualId, long sunrise, long sunset) {
        int id = actualId / 100;
        String icon = "";
        if (actualId == 800) {
            long currentTime = new Date().getTime();
            if (currentTime >= sunrise && currentTime < sunset) {
                icon = getActivity().getString(R.string.weather_sunny);
                wt.setImageDrawable(getResources().getDrawable(R.drawable.calm));
            } else {
                icon = getActivity().getString(R.string.weather_clear_night);
                wt.setImageDrawable(getResources().getDrawable(R.drawable.calmnight));
            }
        } else {
            switch (id) {
                case 2:
                    icon = getActivity().getString(R.string.weather_thunder);
                    wt.setImageDrawable(getResources().getDrawable(R.drawable.thunderstorm));
                    break;
                case 3:
                    icon = getActivity().getString(R.string.weather_drizzle);
                    wt.setImageDrawable(getResources().getDrawable(R.drawable.drizzle));
                    break;
                case 7:
                    icon = getActivity().getString(R.string.weather_foggy);
                    wt.setImageDrawable(getResources().getDrawable(R.drawable.sleet));
                    break;
                case 8:
                    icon = getActivity().getString(R.string.weather_cloudy);
                    wt.setImageDrawable(getResources().getDrawable(R.drawable.cloudy));
                    break;
                case 6:
                    icon = getActivity().getString(R.string.weather_snowy);
                    wt.setImageDrawable(getResources().getDrawable(R.drawable.snow));
                    break;
                case 5:
                    icon = getActivity().getString(R.string.weather_rainy);
                    wt.setImageDrawable(getResources().getDrawable(R.drawable.rain));
                    break;
            }
        }
        weatherIcon.setText(icon);
        progressBar.setVisibility(View.GONE);
        hide.setVisibility(View.GONE);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mScrollView = (ObservableScrollView)view.findViewById(R.id.containers);
        /*RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view3);

        //recyclerView.setHasFixedSize(true);
        _sGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(_sGridLayoutManager);
        sList = getListItemData();
        SampleRecyclerViewAdapter rcAdapter = new SampleRecyclerViewAdapter(getContext(), sList);
        recyclerView.setAdapter(rcAdapter);*/
        MaterialViewPagerHelper.registerScrollView(getActivity(),mScrollView,null);
        updateWeatherData();
        //MaterialViewPagerHelper.registerRecyclerView(getActivity(),recyclerView,null);

    }
/*
    private List<ItemObject> getListItemData()
    {
        List<ItemObject> listViewItems = new ArrayList<ItemObject>();
        listViewItems.add(new ItemObject("Kurnool", "Seemandhra",getResources().getDrawable(R.drawable.kurnool),new LatLng(15.8118442,78.039802)));
        listViewItems.add(new ItemObject("Ziro", "Arunachal Pradesh",getResources().getDrawable(R.drawable.ziro),new LatLng(27.5594944,93.8285208)));
        listViewItems.add(new ItemObject("Tawang", "Arunachal Pradesh",getResources().getDrawable(R.drawable.tawang),new LatLng(27.5739351,91.8718024)));
        listViewItems.add(new ItemObject("Majuli", "Assam",getResources().getDrawable(R.drawable.majuli),new LatLng(27.0074877,94.1946745)));
        listViewItems.add(new ItemObject("Manas National Park", "Assam",getResources().getDrawable(R.drawable.manas),new LatLng(26.6408611,91.0950703)));
        listViewItems.add(new ItemObject("Champaner-Pavagadh", "Gujarat",getResources().getDrawable(R.drawable.champaneer),new LatLng(22.4844952,73.532331)));
        listViewItems.add(new ItemObject("Patan", "Gujarat",getResources().getDrawable(R.drawable.patan),new LatLng(23.8481837,72.1223259)));
        listViewItems.add(new ItemObject("Khajjar", "Himachal Pradesh",getResources().getDrawable(R.drawable.khajjar),new LatLng(32.550361,76.0630535)));
        listViewItems.add(new ItemObject("Shojha", "Himachal Pradesh",getResources().getDrawable(R.drawable.shojha),new LatLng(31.5675117,77.3710012)));
        listViewItems.add(new ItemObject("Kasol", "Himachal Pradesh",getResources().getDrawable(R.drawable.kasol),new LatLng( 32.0098591,77.3143047)));
        return listViewItems;
    }*/
}
