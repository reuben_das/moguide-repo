package com.sample.moguide.santa;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RecyclerViewYoutube2 extends Fragment {
    ObservableScrollView mScrollView;
    ImportData a;


    public StaggeredGridLayoutManager _sGridLayoutManager;
    List<Post> sList;
    String add;
    String phone;
    CardView r;
    CardView post2;
    ArrayList<Post> rev;
    PostAdapter adapter;
    Post post;
    RecyclerView recyclerView;
    TextView ctr;
    RelativeLayout call2;
    LinearLayout tim;
    TextView time;

    public RecyclerViewYoutube2() {
    }

    public RecyclerViewYoutube2(String add, String phone) {
        this.add = add;
        this.phone = phone;
    }

    public RecyclerViewYoutube2(String add, String phone, ImportData a) {
        this.add = add;
        this.phone = phone;
        this.a = a;
    }

    public static RecyclerViewYoutube2 newInstance() {

        return new RecyclerViewYoutube2();
    }
    

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_youtube2, container, false);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mScrollView = (ObservableScrollView)view.findViewById(R.id.containers2);
        MaterialViewPagerHelper.registerScrollView(getActivity(),mScrollView,null);
        TextView address = (TextView) view.findViewById(R.id.address);
        time = (TextView) view.findViewById(R.id.time);
        ctr = (TextView) view.findViewById(R.id.ctr);
        tim = (LinearLayout) view.findViewById(R.id.tm);
        address.setText(add);
        ImageButton call = (ImageButton)view.findViewById(R.id.call);
        call2 = (RelativeLayout) view.findViewById(R.id.call2);

        if (a != null) {

            if (a.getNumber() != null) {
                call2.setVisibility(View.VISIBLE);
                call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String posted_by = a.getNumber() + "";
                        String uri = "tel:" + posted_by.trim();
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(uri));
                        startActivity(intent);
                    }
                });
            } else {
                call2.setVisibility(View.GONE);

            }

            if (a.getTiming() != null) {
                tim.setVisibility(View.VISIBLE);
                time.setText(a.getTiming());
            } else {
                tim.setVisibility(View.GONE);
            }

        }

        RatingBar bar = (RatingBar) view.findViewById(R.id.r2);
        bar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(final RatingBar ratingBar, final float rating, boolean fromUser) {
                AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());
                // Get the layout inflater
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View subView = inflater.inflate(R.layout.dialog_review, null);
                final EditText et = (EditText) subView.findViewById(R.id.title);
                final EditText et2 = (EditText) subView.findViewById(R.id.description);
                builder2.setView(subView)
                        // Add action buttons
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference("moguide_test_data");
                                com.google.firebase.database.Query queryRef = ref2.orderByChild("type").equalTo(a.getPlaces_to_visit());
                                DatabaseReference r3 = queryRef.getRef();
                                //Toast.makeText(getContext(),r3.toString(),Toast.LENGTH_LONG).show();
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                                if (user != null) {
                                    String key = ref.child("reviews").push().getKey();
                                    Post post = new Post(user.getUid(), user.getDisplayName(), et.getText().toString(), et2.getText().toString(), ((int) ratingBar.getRating()));
                                    Map<String, Object> postValues = post.toMap();
                                    Map<String, Object> childUpdates = new HashMap<>();
                                    Map<String, Object> childUpdates2 = new HashMap<>();
                                    childUpdates.put("/reviews/" + a.getPlaces_to_visit() + "/" + user.getUid(), postValues);
                                    ref.updateChildren(childUpdates);
                                    //Toast.makeText(getContext(), a.getKey(), Toast.LENGTH_LONG).show();
                                    switch (((int) ratingBar.getRating())) {
                                        case 1:
                                            childUpdates2.put("/" + a.getKey() + "/rate1", a.getRate1() + 1);
                                            break;

                                        case 2:
                                            childUpdates2.put("/" + a.getKey() + "/rate2", a.getRate2() + 1);
                                            break;

                                        case 3:
                                            childUpdates2.put("/" + a.getKey() + "/rate3", a.getRate3() + 1);
                                            break;

                                        case 4:
                                            childUpdates2.put("/" + a.getKey() + "/rate4", a.getRate4() + 1);
                                            break;

                                        case 5:
                                            childUpdates2.put("/" + a.getKey() + "/rate5", a.getRate5() + 1);
                                            break;
                                    }
                                    ref2.updateChildren(childUpdates2);
                                }
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ratingBar.setRating(0);
                            }
                        });
                AlertDialog dialog2 = builder2.create();
                dialog2.show();
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_viewpost);

        //recyclerView.setHasFixedSize(true);
        //_sGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        //recyclerView.setLayoutManager(_sGridLayoutManager);
        //sList = getListItemData();
        //SampleRecyclerViewAdapter rcAdapter = new SampleRecyclerViewAdapter(getContext(), sList);
        //recyclerView.setAdapter(rcAdapter);

        rev = new ArrayList<>();

        post2 = (CardView) view.findViewById(R.id.postview);
        post2.setVisibility(View.GONE);
        r = (CardView) view.findViewById(R.id.review);
        ctr.setText("See all Reviews");
        r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (post2.getVisibility() == View.GONE) {
                    post2.setVisibility(View.VISIBLE);
                    ctr.setText("Hide Reviews");
                    rev = new ArrayList<Post>();
                    //Toast.makeText(getActivity(),"yoooooooo",Toast.LENGTH_LONG).show();
                    DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference("reviews" + "/" + a.getPlaces_to_visit() + "/");
                    //com.google.firebase.database.Query queryRef = ref2.equalTo(a.getPlaces_to_visit());
                    ref2.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            //Toast.makeText(getActivity(),"yoooooooo2",Toast.LENGTH_LONG).show();
                            post = dataSnapshot.getValue(Post.class);
                            rev.add(post);
                            adapter = new PostAdapter(rev);
                            adapter.notifyDataSetChanged();
                            recyclerView.setAdapter(adapter);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(adapter);

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            //Toast.makeText(getActivity(),"yoooooooo3",Toast.LENGTH_LONG).show();
                            rev.remove(post);
                            post = dataSnapshot.getValue(Post.class);
                            rev.add(post);
                            adapter = new PostAdapter(rev);
                            adapter.notifyDataSetChanged();
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(adapter);

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {
                    post2.setVisibility(View.GONE);
                    ctr.setText("See all Reviews");
                }
            }
        });

    }

}
