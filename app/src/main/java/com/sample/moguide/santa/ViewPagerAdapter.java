package com.sample.moguide.santa;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    LatLng w;
    String content;
    String phone;
    String address;
    ImportData a;

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);
        w = new LatLng(28.5436056, 77.2056723);
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    /*public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, LatLng wr) {
        super(fm);
        this.w = wr;
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    */
    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, LatLng wr, String content, String address, String phone, ImportData a) {
        super(fm);
        this.w = wr;
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.content = content;
        this.address = address;
        this.phone = phone;
        this.a = a;
    }

    /*public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, String content) {
        super(fm);
        w = new LatLng(28.5436056, 77.2056723);
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.content = content;
    }*/

    //This method returns the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0) // if the position is 0 we are returning the First tab
        {
            ArrayList<String> xata1=new ArrayList<>();
            xata1.add("A/V Guide");
            RecyclerViewYoutube tab1;
            if (a != null) {
                tab1 = new RecyclerViewYoutube(w, content, a);
            } else {
                tab1 = new RecyclerViewYoutube(w, content);
            }

            return tab1;
        }
        else            // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            ArrayList<String> xata=new ArrayList<>();
            xata.add("");
            RecyclerViewYoutube2 tab2;
            if (a != null) {
                tab2 = new RecyclerViewYoutube2(address, phone, a);
            } else {
                tab2 = new RecyclerViewYoutube2(address, phone);
            }
            return tab2;
        }
        /*else
        {
            ArrayList<String> xata=new ArrayList<>();
            xata.add("Hare Krishna Hill, Sant Nagar, Main Road, East of Kailash, Sant Nagar, East of Kailash, New Delhi, Delhi 110065");
            xata.add("Phone Number: 9818970154");
            RecyclerViewFragment tab3 = new RecyclerViewFragment(2,xata);
            return tab3;
        }*/
    }

    // This method returns the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method returns the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

    public void updateContent(String content) {
        this.content = content;
        notifyDataSetChanged();
    }

    public void updt(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, LatLng wr, String content, String address, String phone, ImportData a) {
        this.w = wr;
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.content = content;
        this.address = address;
        this.phone = phone;
        this.a = a;
        notifyDataSetChanged();
    }
}
