package com.sample.moguide.santa;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AdapterExp extends BaseExpandableListAdapter {
    private List<String> header_titles;
    private HashMap<String,List<String>> child_titles;
    private Context ctx;
    private int pos;
    ArrayList<Integer> listpos;
    ArrayList<Integer> childpos;
    //private Layout Lay;
    //private int LT;

    AdapterExp(Context ctx, List<String> header_titles, HashMap<String, List<String>> child_titles, ArrayList<Integer> listpos, ArrayList<Integer> childpos) {

        this.listpos=listpos;
        this.ctx=ctx;
        this.header_titles=header_titles;
        this.child_titles=child_titles;
        this.childpos = childpos;
        //this.pos=pos;
        //this.LT=LT;

    }

    @Override
    public int getGroupCount() {
        return header_titles.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return child_titles.get(header_titles.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return header_titles.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return child_titles.get(header_titles.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String title=(String)this.getGroup(groupPosition);
        if(convertView==null){
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=layoutInflater.inflate(R.layout.parent_layout,null);
        }
        ImageView img = (ImageView) convertView.findViewById(R.id.imageViewParent);
        ImageView img2 = (ImageView) convertView.findViewById(R.id.imageViewParent2);
        TextView textView = (TextView) convertView.findViewById(R.id.headlist);
        textView.setText(title);
        switch (title) {
            /*case "Trending":
                img.setImageResource(R.drawable.ic_whatshot_black_48dp);
                img2.setImageDrawable(null);
                //img2.setImageResource(android.R.color.transparent);
                break;*/
            case "Nearby":
                img.setImageResource(R.drawable.ic_near_me_black_24dp);
                img2.setImageDrawable(null);
                //img2.setImageResource(android.R.color.transparent);
                break;
            case "Hostels":
                img.setImageResource(R.drawable.ic_local_hotel_black_24dp);
                if (listpos.get(0) == 1) {
                    img2.setImageResource(R.drawable.monumentstick);
                } else if (listpos.get(0) == 0) {
                    img2.setImageDrawable(null);
                }

                break;
            case "Eateries":
                img.setImageResource(R.drawable.ic_restaurant_black_24dp);
                if (listpos.get(1) == 5) {
                    img2.setImageResource(R.drawable.monumentstick);
                } else if (listpos.get(1) < 5 && listpos.get(1) != 0) {
                    img2.setImageResource(R.drawable.uns);
                } else if (listpos.get(1) == 0) {
                    img2.setImageDrawable(null);
                }
                break;
            case "Sight Seeing":
                img.setImageResource(R.drawable.monuments2);
                if (listpos.get(2) == 5) {
                    img2.setImageResource(R.drawable.monumentstick);
                } else if (listpos.get(2) < 5 && listpos.get(2) != 0) {
                    img2.setImageResource(R.drawable.uns);
                } else if (listpos.get(2) == 0) {
                    img2.setImageDrawable(null);
                }
                break;
            case "Shopping":
                img.setImageResource(R.drawable.shopping_large2);
                if (listpos.get(3) == 4) {
                    img2.setImageResource(R.drawable.shoppingtick);
                } else if (listpos.get(3) < 4 && listpos.get(3) != 0) {
                    img2.setImageResource(R.drawable.uns);
                } else if (listpos.get(3) == 0) {
                    img2.setImageDrawable(null);
                }
                break;
            case "Entertainment":
                img.setImageResource(R.drawable.sight_large2);
                if (listpos.get(4) == 3) {
                    img2.setImageResource(R.drawable.populart);
                } else if (listpos.get(4) < 3 && listpos.get(4) != 0) {
                    img2.setImageResource(R.drawable.uns);
                } else if (listpos.get(4) == 0) {
                    img2.setImageDrawable(null);
                }
                break;
            case "Amenities":
                img.setImageResource(R.drawable.amenlarge4);
                if (listpos.get(5) == 5) {
                    img2.setImageResource(R.drawable.amenitiestick);
                } else if (listpos.get(5) < 5 && listpos.get(5) != 0) {
                    img2.setImageResource(R.drawable.uns);
                } else if (listpos.get(5) == 0) {
                    img2.setImageDrawable(null);
                }
                break;
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String title = (String) this.getChild(groupPosition, childPosition);
        if(convertView==null){
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=layoutInflater.inflate(R.layout.child_layout,null);
        }
        ImageView img3 = (ImageView) convertView.findViewById(R.id.imageViewChild);
        CheckedTextView checkedTextView = (CheckedTextView) convertView.findViewById(R.id.childlist);
        checkedTextView.setText(title);
        switch (title) {
            /*case "Hotels":
                if (childpos.get(0) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(0) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Hostels":
                if (childpos.get(1) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(1) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Resorts":
                if (childpos.get(2) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(2) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Guest Houses":
                if (childpos.get(3) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(3) == 0) {
                    img3.setImageDrawable(null);
                }
                break;*/

            case "Dhaba":
                if (childpos.get(4) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(4) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Multi Cuisine":
                if (childpos.get(5) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(5) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Mughlai":
                if (childpos.get(6) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(6) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Pure Veg":
                if (childpos.get(7) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(7) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Café":
                if (childpos.get(8) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(8) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Monuments":
                if (childpos.get(13) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(13) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Places To Visit":
                if (childpos.get(14) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(14) == 0) {
                    img3.setImageDrawable(null);
                }

                break;

            case "Museums":
                if (childpos.get(15) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(15) == 0) {
                    img3.setImageDrawable(null);
                }

                break;

            case "Religious Spots":
                if (childpos.get(16) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(16) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Parks":
                if (childpos.get(9) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(9) == 0) {
                    img3.setImageDrawable(null);
                }
                break;


            case "Malls":
                if (childpos.get(17) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(17) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Local Market":
                if (childpos.get(18) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(18) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Flea Market":
                if (childpos.get(19) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(19) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Handicrafts":
                if (childpos.get(20) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(20) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Adventure Sports":
                if (childpos.get(21) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(21) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Water Parks":
                if (childpos.get(22) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(22) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Zoo":
                if (childpos.get(23) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(23) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Car/Bike Rentals":
                if (childpos.get(24) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(24) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Hospitals":
                if (childpos.get(25) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(25) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Pharmacy":
                if (childpos.get(26) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(26) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "ATM":
                if (childpos.get(27) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(27) == 0) {
                    img3.setImageDrawable(null);
                }
                break;

            case "Petrol Pump":
                if (childpos.get(28) == 1) {
                    img3.setImageResource(R.drawable.uns);
                } else if (childpos.get(28) == 0) {
                    img3.setImageDrawable(null);
                }
                break;
        }
        //checkedTextView.setCheckMarkDrawable(R.drawable.chk2);
        String parentname=getGroup(groupPosition).toString();
        //set image

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

   public void updateAdapter(ArrayList<Integer> listpos)
   {
       this.listpos=listpos;
       notifyDataSetChanged();
   }

   public void updateChild(ArrayList<Integer> childpos)
   {
       this.childpos=childpos;
       notifyDataSetChanged();
   }


}
