package com.sample.moguide.santa;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());

                //  Create a new boolean and preference and set it to true
                boolean isFirstStart = getPrefs.getBoolean("firstStartS", true);

                //  If the activity has never started before...
                if (isFirstStart) {
                    try {
                        Thread.sleep(400);
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //  Initialize SharedPreferences
                                SharedPreferences getPrefs = PreferenceManager
                                        .getDefaultSharedPreferences(getBaseContext());

                                //  Create a new boolean and preference and set it to true
                                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);

                                //  If the activity has never started before...
                                if (isFirstStart) {

                                    //  Launch app intro
                                    Intent i = new Intent(Splash.this, MyIntro.class);
                                    startActivity(i);

                                    //  Make a new preferences editor
                                    SharedPreferences.Editor e = getPrefs.edit();

                                    //  Edit preference to make it false because we don't want this to run again
                                    e.putBoolean("firstStart", false);

                                    //  Apply changes
                                    e.apply();
                                }

                            }
                        });

                        // Start the thread
                        thread.start();
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }

                    SharedPreferences.Editor e = getPrefs.edit();
                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStartS", false);

                    //  Apply changes
                    e.apply();
                }
                else{
                    Intent i=new Intent(Splash.this,Login2.class);
                    startActivity(i);
                }
            }
        });
        t.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

}