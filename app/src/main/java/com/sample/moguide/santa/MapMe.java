package com.sample.moguide.santa;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;



/**
 * A simple {@link Fragment} subclass.
 */


public class MapMe extends Fragment implements GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    //int check = 0;
    public boolean itinDragged = false;
    LatLng removedLoc, addedLoc;
    Polyline polyline;
    //Marker marker2;
    MainActivity mainRefrence;
    Location mLastLocation;
    //PolylineOptions draw;
    //MainActivity mainActivity = new MainActivity();
    MapView mMapView;
    GoogleMap googleMap;
    UiSettings uiSettings;
    String sk;
    double distancematrix[][];
    //Location glob;

    LocationManager lm;

    ArrayList<String> names;

    String sk2;
    //int skch=0;

    //ImportData importData2;

    LatLng loc;

    Place p1;
    Place p2;

    Marker mark;
    LatLng g;

    ArrayList<Marker> testmarks;
    ArrayList<Marker> testmarks2;

    //String pk;

    //int flag_rlf=0;

    //MarkerOptions marker;
    //int flag = 0;

    int flag2 = 0;

    int slide = 0;
    //int slide2 = 0;
    GoogleApiClient mGoogleApiClient;
    //boolean ret;
    //int cnt = 0;
    RealItinerary realItinerary;
    //LatLng x;
    LinkedList<RealItinerary> init;

    ArrayList<Integer> icons_mark;

    MarkerOptions glb;

    ArrayList<String> added2;

    FloatingActionMenu fab;
    FloatingActionButton fab1;
    FloatingActionButton fab2;

    ArrayList<PolylineOptions> savedItems;
    ArrayList<MarkerOptions> savedMarkers;

    HashMap<String, MarkerOptions> baseMark;
    HashMap<String, BitmapDescriptor> baseIcon;

    //ArrayList<Marker> basemarkers;

    //String[] str;

    double latitude;
    double longitude;

    ProgressBar progressBar;
    ProgressBar progressBar2;

    //public String[] Finally;

    HashMap<String, String> untouch;
    PolylineOptions polyLineOptions;
    //PolygonOptions rectOptions;
    MarkerOptions markerOptions;
    //Polygon polygon;
    int count;
    //ProgressBar pb;
    LinkedList<LatLng> unsorted;
    //TinyDB tinydb;
    //final String TAG = "PathGoogleMapActivity";
    Menu m;
    SubMenu topChannelMenu;
    SubMenu botm;

    HashMap<String, LatLng> crnt = new HashMap<>();

    public MapMe() {

    }

    public MapMe(MainActivity main) {
        mainRefrence = main;
        realItinerary = new RealItinerary(this);
        loc = new LatLng(28.5436056, 77.2056723);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_location_info, container,
                false);

        return v;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        m = mainRefrence.navigationView.getMenu();

        untouch = new HashMap<>();
        topChannelMenu = m.addSubMenu("Saved Itineraries");
        botm = m.addSubMenu("Communicate");
        botm.add("Share (Coming Soon)").setIcon(R.drawable.share);
        botm.add("Book a Cab (Coming Soon)").setIcon(R.drawable.ola3);
        botm.add("").collapseActionView();
        //m.addSubMenu()
        added2 = new ArrayList<>();

        baseMark = new HashMap<>();
        baseIcon = new HashMap<>();

        //mark.setSnippet("Yo");
        //tinydb = new TinyDB(getContext());
        savedItems = new ArrayList<>();
        savedMarkers = new ArrayList<>();
        testmarks= new ArrayList<>();
        icons_mark= new ArrayList<>();
        //icons_mark.add(R.drawable.ic_room_black_48dp);
        testmarks2= new ArrayList<>();

        fab = (FloatingActionMenu)view.findViewById(R.id.menu_yellow);
        fab.setVisibility(View.INVISIBLE);
        fab1= (FloatingActionButton)view.findViewById(R.id.fab1);
        fab2= (FloatingActionButton)view.findViewById(R.id.fab2);
        fab1.setOnClickListener(clickListener);
        fab2.setOnClickListener(clickListener);

        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        progressBar2 = (ProgressBar) view.findViewById(R.id.progressBar2);
        progressBar2.setVisibility(View.GONE);


        //markerOptions.getIcon();

        count = 1;
        init = new LinkedList<RealItinerary>();
        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        unsorted = new LinkedList<>();
        //pb = (ProgressBar) v.findViewById(R.id.progressBar1);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);
        //googleMap.getUiSettings();


    }

    public void setupmap() {

        uiSettings = googleMap.getUiSettings();


        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setMapToolbarEnabled(false);
        googleMap.setMyLocationEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        } else {
            //googleMap.setMyLocationEnabled(true);
            // Show rationale and request permission.
        }


        if(mLastLocation!=null)
        {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
        }
        else {
            latitude = 28.5436056;
            longitude = 77.2056723;
        }

        // create marker

        //polyline= googleMap.addPolyline(draw);
        googleMap.setOnMarkerClickListener(this);

        googleMap.setOnMapLongClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);
        googleMap.setOnMapClickListener(this);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putAll();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public boolean onMyLocationButtonClick() {

        boolean gps_enabled = false;
        boolean network_enabled = false;
        lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage("Enable your Location Services");
            dialog.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                }
            });
            dialog.show();
        }
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        glb = new MarkerOptions().snippet(marker.getSnippet());
        mark = marker;

        loc = marker.getPosition();
        sk = marker.getTitle();
        ImportData a = setPin(sk);
        if (a != null) {
                mainRefrence.dragview.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mainRefrence.mLayout.setTouchEnabled(true);
                final String u1 = a.getImage_URL1();
                final String u2 = a.getImage_URL2();
            //mainRefrence.mViewPager.notifyHeaderChanged();
            //mainRefrence.mViewPager.setImageUrl(u1,0);
            //mainRefrence.mViewPager.onPageSelected(0);
            mainRefrence.setWeatherAdapter(loc, a.getContent(), a.getAddress(), "911", a);
                mainRefrence.mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
                    @Override
                    public HeaderDesign getHeaderDesign(int page) {
                        switch (page) {
                            case 0:
                                return HeaderDesign.fromColorResAndUrl(R.color.colorPrimary, u1);
                            case 1:
                                if (u2 != null) {
                                    return HeaderDesign.fromColorResAndUrl(R.color.colorPrimary, u2);
                                } else {
                                    return HeaderDesign.fromColorResAndUrl(R.color.colorPrimary, u1);
                                }
                        }
                        //execute others actions if needed (ex : modify your header logo)
                        return null;
                    }
                });
            mainRefrence.mViewPager.getViewPager().setCurrentItem(1);
            mainRefrence.mViewPager.getViewPager().setCurrentItem(0);


        } else {
            mainRefrence.mLayout.setTouchEnabled(false);
            mainRefrence.dragview.setBackgroundColor(getResources().getColor(R.color.indigo));
        }


        if (slide == 0) {
            mainRefrence.setPanelOn(marker.getTitle());
            fab.setVisibility(View.INVISIBLE);
            uiSettings.setZoomControlsEnabled(false);
            g = loc;
            slide = 1;
        } else if (slide == 1 && ((g.latitude == loc.latitude) && (g.longitude == loc.longitude))) {
            mainRefrence.setPanelOff();
            if(mainRefrence.locations>0) {
                fab.setVisibility(View.VISIBLE);

            }
            uiSettings.setZoomControlsEnabled(true);
            slide = 0;
        } else if (slide == 1 && ((g.latitude != loc.latitude) && (g.longitude != loc.longitude))) {
            mainRefrence.setPanelOn(marker.getTitle());
            fab.setVisibility(View.INVISIBLE);
            uiSettings.setZoomControlsEnabled(false);
            g = loc;
            slide = 1;
        }

        return false;
    }

    public void navi()
    {
        if(mLastLocation.getLatitude()==loc.latitude){

        }
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+loc.latitude+","+loc.longitude);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    public void navi(int check,LatLng ln){
        mainRefrence.check_navigate=0;
        if(check==1)
        {
            Uri gmmIntentUri = Uri.parse("google.navigation:q="+ln.latitude+","+ln.longitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    }
    @Override
    public void onMapLongClick(LatLng latLng) {
        MarkerOptions marker = new MarkerOptions().position(latLng).title("NEW ADDED");
        sk = marker.getTitle();
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        marker.snippet("Long");
        googleMap.addMarker(marker);
        baseMark.put(marker.getTitle().toString(), marker);
        baseIcon.put(marker.getTitle().toString(), marker.getIcon());


    }

    public void addTrend(String name,LatLng lc){
        MarkerOptions marker = new MarkerOptions().position(lc).title(name);
        sk = marker.getTitle();
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        marker.snippet("Long");
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(lc).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        mainRefrence.trendy.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        //mainRefrence.setPanelExpanded();
    }


    public LatLng retItem(int position) {
        return (realItinerary.sorted.get(position));
    }


    public void dragItem(int fromPosition, int toPosition) {
        itinDragged = true;

        if(fromPosition==toPosition)
            return;

        if (polyline != null) {
            polyline.remove();
        }
        LatLng item = realItinerary.sorted.remove(fromPosition);
        realItinerary.sorted.add(toPosition, item);
        String name = realItinerary.destination.remove(fromPosition);
        realItinerary.destination.add(toPosition, name);
        String url = getMapsApiDirectionsUrl(realItinerary.sorted.get(0), realItinerary.sorted.get(1), realItinerary.sorted);
        ReadTask readTask = new ReadTask();
        readTask.execute(url);
    }

    public void addItem() {
        addedLoc = loc;
        if(mark!=null) {
            testmarks.add(mark);
            icons_mark.add(1);

        }

        if (polyline != null) {
            polyline.remove();
        }
        if (unsorted == null) {
            unsorted = new LinkedList<LatLng>();
        }
        unsorted.add(new LatLng(addedLoc.latitude, addedLoc.longitude));
        if (unsorted.size() == 2) {
            untouch.put(sk, sk);
            realItinerary.addDestination(sk, addedLoc.latitude, addedLoc.longitude, init, null);
            String url = getMapsApiDirectionsUrl(unsorted.get(0), unsorted.get(1), realItinerary.sorted);
            //Toast.makeText(getContext(), 1 + "", Toast.LENGTH_SHORT).show();
            ReadTask task = new ReadTask();
            task.execute(url);

        } else if (unsorted.size() > 2) {
            untouch.put(sk, sk);
            String url = getMapsDistanceMatrixUrl();
            DistanceMatrixReadTask task = new DistanceMatrixReadTask();
            //Toast.makeText(getContext(), 2 + "", Toast.LENGTH_SHORT).show();
            task.execute(url);
        } else {
            untouch.put(sk, sk);
            realItinerary.addDestination(sk, addedLoc.latitude, addedLoc.longitude, init, null);
            Toast.makeText(getContext(), realItinerary.destination.getLast() + " added", Toast.LENGTH_SHORT).show();
            //Toast.makeText(getContext(), 3 + "", Toast.LENGTH_SHORT).show();
        }
    }

    public void addItem(LatLng Loc2) {
        testmarks.add(mark);
        addedLoc = Loc2;
        icons_mark.add(0);
        if (polyline != null) {
            polyline.remove();
        }
        unsorted.add(new LatLng(addedLoc.latitude, addedLoc.longitude));
        if (unsorted.size() == 2) {
            untouch.put(sk, sk);
            realItinerary.addDestination("My Location", addedLoc.latitude, addedLoc.longitude, init, null);
            String url = getMapsApiDirectionsUrl(unsorted.get(0), unsorted.get(1), realItinerary.sorted);
            ReadTask task = new ReadTask();
            task.execute(url);
            //Toast.makeText(getContext(), 4 + "", Toast.LENGTH_SHORT).show();

        } else if (unsorted.size() > 2) {
            untouch.put(sk, sk);
            String url = getMapsDistanceMatrixUrl();
            DistanceMatrixReadTask task = new DistanceMatrixReadTask();
            //Toast.makeText(getContext(), 5 + "", Toast.LENGTH_SHORT).show();
            task.execute(url);
        } else {
            untouch.put(sk, sk);
            realItinerary.addDestination("My Location", addedLoc.latitude, addedLoc.longitude, init, null);
            Toast.makeText(getContext(), realItinerary.destination.getLast() + " added ", Toast.LENGTH_SHORT).show();
            //Toast.makeText(getContext(), 6 + "", Toast.LENGTH_SHORT).show();
        }
    }

    public void addItem(LatLng Loc2, int i, String name) {
        int stab = i;
        testmarks.add(mark);
        addedLoc = Loc2;
        icons_mark.add(0);
        System.out.println(stab);
        if (polyline != null) {
            polyline.remove();
        }
        unsorted.add(new LatLng(addedLoc.latitude, addedLoc.longitude));
        if (unsorted.size() == 2) {
            untouch.put(sk, sk);
            realItinerary.addDestination(name, addedLoc.latitude, addedLoc.longitude, init, null);
            String url = getMapsApiDirectionsUrl(unsorted.get(0), unsorted.get(1), realItinerary.sorted);
            ReadTask task = new ReadTask();
            //Toast.makeText(getContext(), 7 + "", Toast.LENGTH_SHORT).show();
            task.execute(url);

        } else if (unsorted.size() > 2) {
            untouch.put(sk, sk);
            String url = getMapsDistanceMatrixUrl();
            DistanceMatrixReadTask task = new DistanceMatrixReadTask();
            //Toast.makeText(getContext(), 8 + "", Toast.LENGTH_SHORT).show();
            task.execute(url);
        } else {
            untouch.put(sk, sk);
            realItinerary.addDestination(name, addedLoc.latitude, addedLoc.longitude, init, null);
            //realItinerary.addDestination(sk, addedLoc.latitude, addedLoc.longitude, init, null);
            Toast.makeText(getContext(), realItinerary.destination.getLast() + " added ", Toast.LENGTH_SHORT).show();
            //Toast.makeText(getContext(), 9 + "", Toast.LENGTH_SHORT).show();
        }
    }

    public void addItem(LatLng Loc2, String name) {
        testmarks.add(mark);
        addedLoc = Loc2;
        icons_mark.add(0);
        if (polyline != null) {
            polyline.remove();
        }
        unsorted.add(new LatLng(addedLoc.latitude, addedLoc.longitude));
        if (unsorted.size() == 2) {
            untouch.put(sk, sk);
            realItinerary.addDestination(name, addedLoc.latitude, addedLoc.longitude, init, null);
            String url = getMapsApiDirectionsUrl(unsorted.get(0), unsorted.get(1), realItinerary.sorted);
            ReadTask task = new ReadTask();
            task.execute(url);
            //Toast.makeText(getContext(), 10 + "", Toast.LENGTH_SHORT).show();

        } else if (unsorted.size() > 2) {
            untouch.put(sk, sk);
            String url = getMapsDistanceMatrixUrl();
            DistanceMatrixReadTask task = new DistanceMatrixReadTask();
            task.execute(url);

        } else {
            untouch.put(sk, sk);
            realItinerary.addDestination(name, addedLoc.latitude, addedLoc.longitude, init, null);
            //realItinerary.addDestination(sk, addedLoc.latitude, addedLoc.longitude, init, null);
            Toast.makeText(getContext(), realItinerary.destination.getLast() + " added ", Toast.LENGTH_SHORT).show();
        }
    }


    public void removeItem() {
        boolean removeCheck=false; int i;
        for(i=0;i<unsorted.size();i++)
        {
            if((unsorted.get(i).latitude==loc.latitude)&&(unsorted.get(i).longitude==loc.longitude)){
                unsorted.remove(i);
                removeCheck=true;
                break;
            }
        }
        removedLoc = loc;


        if (removeCheck) {
            if (polyline != null) {
                polyline.remove();
            }
            if (unsorted.size() >= 2) {
                untouch.remove(sk);
                changedMatrixOnRemoveItem(distancematrix,i);
            }
            else {
                untouch.remove(sk);
                realItinerary.removeDestination(((sk == null) || (sk == "NEW ADDED")) ? "Waypoint 1" : sk, removedLoc.latitude, removedLoc.longitude, init, null);
            }
            Toast.makeText(getContext(), "Removed", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getContext(), "Removal failed", Toast.LENGTH_SHORT).show();

    }

    public void removeItem(LatLng loca) {
        boolean removeCheck=false; int i;
        for(i=0;i<unsorted.size();i++)
        {
            if((unsorted.get(i).latitude==loca.latitude)&&(unsorted.get(i).longitude==loca.longitude)){
                unsorted.remove(i);
                removeCheck=true;
                break;
            }
        }
        removedLoc = loca;
        Toast.makeText(getContext(), "Removed: " + removeCheck, Toast.LENGTH_SHORT).show();
        if (removeCheck) {
            if (polyline != null) {
                polyline.remove();
            }
            if (unsorted.size() >= 2) {
                untouch.remove(sk);
                changedMatrixOnRemoveItem(distancematrix,i);
            }  else {
                untouch.remove(sk);
                realItinerary.removeDestination(((sk == null) || (sk == "NEW ADDED")) ? "Waypoint 1" : sk, loca.latitude, loca.longitude, init, null);
            }
        }
    }
    public void changedMatrixOnRemoveItem(double[][] adMatrix, int index)
    {   int l=adMatrix[1].length-1,m=1,n=1;
        double [][]newAdMatrix=new double[l][l];
        for(int i=1;i<=l;i++)
        {   if(i==(index+1))
            continue;
            for(int j=1;j<=l;j++){
                if(j==(index+1))
                    continue;
                else{
                    System.out.println("The number "+adMatrix[i][j]+" removed at index "+i+", "+j+ "and added to new array at index "+m+", "+n);
                    newAdMatrix[m][n]
                            =
                            adMatrix[i][j];
                    n++;
                }
            }
            m++;n=1;
        }
        distancematrix=newAdMatrix;
        realItinerary.removeDestination(sk, removedLoc.latitude, removedLoc.longitude, init, newAdMatrix);

        System.out.println(realItinerary.sorted + ":Sorted");
        LatLng Origin = realItinerary.sorted.get(0);
        LatLng Destination = realItinerary.sorted.get(realItinerary.sorted.size() - 1);
        System.out.println(realItinerary.destination);
        String url = getMapsApiDirectionsUrl(Origin, Destination, realItinerary.sorted);
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);
    }


    @Override
    public void onMapClick(LatLng latLng) {
        mainRefrence.setPanelOff();
        if(mainRefrence.locations>0)
        {
            fab.setVisibility(View.VISIBLE);
        }
        uiSettings.setZoomControlsEnabled(true);
        slide = 0;
    }

    private String getMapsDistanceMatrixUrl() {
        String init = "https://maps.googleapis.com/maps/api/distancematrix/json?";
        String origins = "";
        String destinations = "";
        if (unsorted.size() > 1) {
            origins = "origins=";
            destinations = "destinations=";
            for (int i = 0; i < unsorted.size(); i++) {
                if (i == unsorted.size() - 1) {
                    origins += unsorted.get(i).latitude + "," + unsorted.get(i).longitude + "&";
                    destinations += unsorted.get(i).latitude + "," + unsorted.get(i).longitude + "&";
                } else {
                    origins += unsorted.get(i).latitude + "," + unsorted.get(i).longitude + "|";
                    destinations += unsorted.get(i).latitude + "," + unsorted.get(i).longitude + "|";
                }
            }
            String key = "key=AIzaSyCbuNQvdDA2xw_IGnDPcL1ceArkP2BZkLQ";
            String url = init + origins + destinations + key;
            System.out.println("Distance Matrix Url: " + url);
            return url;
        } else {
            return "";
        }
    }


    public double[][] getDistanceMatrix(JSONObject jObj) {

        double getdistance = 0;
        System.out.println("Distance matrix: ");
        double distanceMatrixArray[][] = new double[unsorted.size() + 1][unsorted.size() + 1];
        try {
            JSONArray rowsArray = jObj.getJSONArray("rows");
            for (int i = 0; i < rowsArray.length(); i++) { // Loop over each each row

                JSONObject row = rowsArray.getJSONObject(i); // Get row object
                JSONArray elements = row.getJSONArray("elements"); // Get all elements for each row as an array
                for (int j = 0; j < elements.length(); j++) { // Iterate each element in the elements array

                    JSONObject element = elements.getJSONObject(j); // Get the element object
                    JSONObject distance = element.getJSONObject("distance"); // Get distance sub object
                    getdistance = distance.getInt("value") / 1000.0;
                    System.out.print(getdistance + "  ");
                    distanceMatrixArray[i + 1][j + 1] = getdistance;
                }
                System.out.println();
            }
        } catch (JSONException e) {
            System.out.println("Error: Json Exception while parsing");
        }
        distancematrix = distanceMatrixArray;
        return distanceMatrixArray;
    }


    private String getMapsApiDirectionsUrl(LatLng Origin, LatLng Destination, ArrayList<LatLng> sort) {
        String waypoints = "";
        if (sort.size() > 2) {
            waypoints = "waypoints=";
            for (int i = 1; i < sort.size() - 1; i++) {
                if (i == sort.size() - 2)
                    waypoints += sort.get(i).latitude + "," + sort.get(i).longitude + "&";
                else
                    waypoints += sort.get(i).latitude + "," + sort.get(i).longitude + "|";
            }
        }

        System.out.println(waypoints);

        //String sensor = "sensor=false";
        //String params = waypoints + "&" + sensor;
        //String output = "json";
        //String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + params;


        String init = "https://maps.googleapis.com/maps/api/directions/json?";
        String origin = "origin=" + Origin.latitude + "," + Origin.longitude + "&";
        String destination = "destination=" + Destination.latitude + "," + Destination.longitude + "&";
        String key = "key=" + "AIzaSyDtFIYxRQVgvtviW8e_77PpNnekYDXN4bk";
        String url = init + origin + destination + waypoints + key;
        System.out.println(url);
        //url="https://maps.googleapis.com/maps/api/directions/json?origin=28.5436056,77.2056723&destination=28.6129167,77.227321&key=AIzaSyDtFIYxRQVgvtviW8e_77PpNnekYDXN4bk";
        return url;
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (crnt.containsKey("My Location")) {

            if (mLastLocation != null) {
                loc = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                MarkerOptions marker = new MarkerOptions().position(
                        loc).title("My Location");
                //addItemsCluster(28.5559017, 77.2517544);

                // Changing marker icon
                marker.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                // adding marker
                glb = marker;
                googleMap.addMarker(marker);
                baseMark.put(marker.getTitle().toString(), marker);
                baseIcon.put(marker.getTitle().toString(), marker.getIcon());


            }

        } else {
            if (mLastLocation != null) {
                loc = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                MarkerOptions marker = new MarkerOptions().position(
                        loc).title("My Location");
                //addItemsCluster(28.5559017, 77.2517544);

                // Changing marker icon
                marker.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                // adding marker
                glb = marker;
                googleMap.addMarker(marker);
                baseMark.put(marker.getTitle().toString(), marker);
                baseIcon.put(marker.getTitle().toString(), marker.getIcon());
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(loc).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));

            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setupmap();
        setUpItems();
    }


    private class ReadTask extends AsyncTask<String, Integer, String> {

        //int count = 0;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }


    }

    public void setSearchResult(Place place, int i) {
        if (i == 1) {
            p1 = place;
        } else if (i == 2) {
            p2 = place;
        }
        MarkerOptions marker = new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()).snippet("Search");
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        googleMap.addMarker(marker);
        baseMark.put(place.getName().toString(), marker);
        baseIcon.put(place.getName().toString(), marker.getIcon());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(place.getLatLng()).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }

    public void setSearchResult(Place place) {
        if(flag2==0){
            p1=place;
        }else{
            p2=place;
        }
        sk=place.getName().toString();
        loc = place.getLatLng();
        MarkerOptions marker = new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()).snippet("Search");
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        googleMap.addMarker(marker);
        baseMark.put(place.getName().toString(), marker);
        baseIcon.put(place.getName().toString(), marker.getIcon());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(place.getLatLng()).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }

    public void setSearchResult(LatLng ltlng) {
        //sk="My Location";
        MarkerOptions marker = new MarkerOptions().position(ltlng).title("Start Location").snippet("Search");
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(ltlng).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            polyLineOptions = null;
            String distance = "";
            int duration = 0;
            int sumDuration = 0;
            System.out.println(routes);
            realItinerary.durationArray = new LinkedList<>();
            polyLineOptions = new PolylineOptions();
            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();

                List<HashMap<String, String>> path = routes.get(i);
                System.out.println("Path("+i+"):"+path);
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    }
                    if (j==1) {
                        duration = Integer.parseInt((String)point.get("duration"));
                        sumDuration+=duration;
                        if (duration >= 3600) {
                            int hrs = (int) (duration / 3600);
                            int min = (int) (((int) (duration - (3600 * hrs))) / 60);
                            int sec = (int) (duration % 60);
                            realItinerary.durationArray.add(hrs + " hr " + min + " m " + sec + "s");
                        } else {
                            realItinerary.durationArray.add((duration / 60) + " m " + (duration % 60) + " s");
                        }

                        System.out.println("Duration: "+duration+", sumDuration: " +sumDuration);
                        continue;
                    }
                    try {
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    } catch (NullPointerException e) {
                        System.out.println("Hashmap path's size is " + path.size());
                        System.out.println("In HashMap<String,String> path, NullPointerException error occured at index " + j);
                    }
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(14);
                polyLineOptions.color(Color.BLUE);

            }

            polyline = googleMap.addPolyline(polyLineOptions);

            if (points != null) {
                mainRefrence.setFlags3(polyLineOptions, null, null);
                mainRefrence.setFlags5(polyLineOptions, null, null);
                //Toast.makeText(getContext(),""+points.size(),Toast.LENGTH_LONG).show();
                /*for(int i = 0;i<points.size();i++)
                {
                    if(i%150==0){
                        mainRefrence.setFlags2(1000,points.get(i).latitude,points.get(i).longitude);
                        //mainRefrence.new GetPlaces(mainRefrence, "atm",5,points.get(i).latitude,points.get(i).longitude,1500).execute();
                    }
                }*/
            }


            boolean hasPoints = false;
            Double maxLat = null, minLat = null, minLon = null, maxLon = null;

            if (polyline.getPoints() != null) {
                List<LatLng> pts = polyline.getPoints();
                for (LatLng coordinate : pts) {
                    // Find out the maximum and minimum latitudes & longitudes
                    // Latitude
                    maxLat = maxLat != null ? Math.max(coordinate.latitude, maxLat) : coordinate.latitude;
                    minLat = minLat != null ? Math.min(coordinate.latitude, minLat) : coordinate.latitude;

                    // Longitude
                    maxLon = maxLon != null ? Math.max(coordinate.longitude, maxLon) : coordinate.longitude;
                    minLon = minLon != null ? Math.min(coordinate.longitude, minLon) : coordinate.longitude;

                    hasPoints = true;
                }
            }

            if (hasPoints) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(new LatLng(maxLat, maxLon));
                builder.include(new LatLng(minLat, minLon));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 60));
            }


            if(realItinerary.destination.size()==2) {
                realItinerary.distancebw.clear();
                realItinerary.distancebw.add(Double.valueOf(distance));
            }
            realItinerary.duration = sumDuration;
            if (realItinerary.duration >= 3600) {
                int hrs = (int) (realItinerary.duration / 3600);
                int min = (int) (((int) (realItinerary.duration - (3600 * hrs))) / 60);
                int sec = (int) (realItinerary.duration % 60);
                realItinerary.dura = hrs + " hr " + min + " m " + sec + "s";
            } else {
                realItinerary.dura = (realItinerary.duration / 60) + " m " + (realItinerary.duration % 60) + " s";
            }
            progressBar.setVisibility(View.GONE);
        }
    }

    private class DistanceMatrixReadTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            DistanceMatrixParserTask dmParserTask = new DistanceMatrixParserTask();
            dmParserTask.execute(result);
        }

    }

    private class DistanceMatrixParserTask extends
            AsyncTask<String, Integer, double[][]> {

        @Override
        protected double[][] doInBackground(
                String... jsonData) {

            JSONObject jObject;
            double[][] distmat = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                distmat = getDistanceMatrix(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return distmat;
        }

        @Override
        protected void onPostExecute(double[][] distmat) {
            count++;
            realItinerary.addDestination(sk, addedLoc.latitude, addedLoc.longitude, init, distmat);
            System.out.println(realItinerary.sorted + ":Sorted");
            if (realItinerary.sorted.size() > 1) {
                LatLng Origin = realItinerary.sorted.get(0);
                LatLng Destination = realItinerary.sorted.get(realItinerary.sorted.size() - 1);
                System.out.println(realItinerary.destination);
                String url = getMapsApiDirectionsUrl(Origin, Destination, realItinerary.sorted);
                ReadTask downloadTask = new ReadTask();
                downloadTask.execute(url);
            }
        }
    }



    public void updatecam() {
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(11f), 1000, null);
    }


    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    public View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            switch (v.getId()) {
                case R.id.fab1:
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());
                    // Get the layout inflater
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View subView = inflater.inflate(R.layout.dialog_save, null);
                    final EditText et = (EditText)subView.findViewById(R.id.usernamesave);
                    builder2.setView(subView)
                            // Add action buttons
                            .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    topChannelMenu.add(et.getText().toString()).setIcon(R.drawable.my_itinerary);
                                    added2.add(et.getText().toString());
                                    savedItems.add(polyLineOptions);
                                    savedMarkers.add(markerOptions);
                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                    DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/");
                                    String key = ref2.child("items").push().getKey();
                                    System.out.println(key);
                                    Map<String, Object> childUpdates = new HashMap<>();

                                    List<String> mItems = new ArrayList<>();
                                    mItems.addAll(realItinerary.destination);
                                    int j = 0;

                                    while (j < mItems.size()) {
                                        String k = mItems.get(j);
                                        MarkerOptions m = baseMark.get(k);
                                        Item yolo = new Item(m.getTitle(), m.getPosition().latitude, m.getPosition().longitude);
                                        Map<String, Object> childUpdates2 = yolo.toMap();
                                        childUpdates.put("/items/" + et.getText() + "/" + j + "/", childUpdates2);
                                        //Toast.makeText(getContext(), m.getTitle(), Toast.LENGTH_SHORT).show();
                                        j++;
                                    }
                                    ref2.updateChildren(childUpdates);

                                    /*com.google.firebase.database.Query queryRef = ref2.orderByChild("name").equalTo(user.getUid());
                                    queryRef.addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });*/
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    AlertDialog dialog2 = builder2.create();
                    dialog2.show();


                    break;
                case R.id.fab2:
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Do you want to clear the map ?");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //setupmap();
                            unsorted.clear();
                            googleMap.clear();
                            if (polyline != null) {
                                polyline.remove();
                                polyline = null;
                                polyLineOptions = null;
                            }
                            //mainRefrence.setup();
                            //realItinerary = new RealItinerary(mainRefrence.mapC);
                            unsorted = new LinkedList<LatLng>();
                            mainRefrence.jrch = 0;
                            mainRefrence.start_flag = 0;
                            mainRefrence.end_flag = 0;
                            mainRefrence.locations = 0;
                            mainRefrence.mapNameBase.clear();
                            mainRefrence.childpos.clear();
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);
                            mainRefrence.childpos.add(0);

                            mainRefrence.listpos.clear();
                            mainRefrence.listpos.add(0);
                            mainRefrence.listpos.add(0);
                            mainRefrence.listpos.add(0);
                            mainRefrence.listpos.add(0);
                            mainRefrence.listpos.add(0);
                            mainRefrence.listpos.add(0);

                            mainRefrence.adapterExp.updateChild(mainRefrence.childpos);
                            mainRefrence.adapterExp.updateAdapter(mainRefrence.listpos);

                            realItinerary = new RealItinerary(mainRefrence.mapC);
                            init = new LinkedList<RealItinerary>();
                            //mainRefrence.setup();

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    break;
            }
        }
    };

    /*public void SV5(){
        if(mainRefrence.sv_5==1) {
            mainRefrence.setPanelOff();
            fab.setVisibility(View.VISIBLE);
            fab.open(true);
            mainRefrence.sv5 = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .setTarget(new ViewTarget(R.id.fab2, getActivity())) //Here is where you supply the id of the action bar item you want to display
                    .setContentTitle("Save your itinerary or Clear the Map")
                    .setStyle(R.style.CustomShowcaseTheme)
                    .build();
            mainRefrence.sv5.setButtonText("Hide");
            mainRefrence.sv5.hideButton();
            mainRefrence.sv_5++;
        }
    }*/

    public ImportData setPin(String title) {
        ImportData d = mainRefrence.mapdata2.get(title);
        return d;
    }

    public void loadPolySaved(String n) {
        fab.setVisibility(View.VISIBLE);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/items/" + n + "/");
        Toast.makeText(getContext(), "Loading " + n, Toast.LENGTH_LONG).show();
        ref2.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Item item = dataSnapshot.getValue(Item.class);
                Marker m = googleMap.addMarker(new MarkerOptions().position(new LatLng(item.getLat(), item.getLon())).title(item.getTitle()));
                //m.setIcon(baseIcon.get(item.getTitle()));
                mainRefrence.mrkr.put(item.getTitle(), m);
                addItem(new LatLng(item.getLat(), item.getLon()), item.getTitle());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setUpItems() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        names = new ArrayList<>();

        DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/items/");
        ref2.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                names.add(dataSnapshot.getKey());
                //Toast.makeText(getContext(), dataSnapshot.getKey(), Toast.LENGTH_SHORT).show();
                topChannelMenu.add(dataSnapshot.getKey()).setIcon(R.drawable.my_itinerary);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
