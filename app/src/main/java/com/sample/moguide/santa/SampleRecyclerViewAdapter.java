package com.sample.moguide.santa;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Reuben on 06/05/16.
 */
public class SampleRecyclerViewAdapter extends RecyclerView.Adapter<SampleViewHolders>
{
    private List<ItemObject> itemList;
    private Context context;
    public MainActivity mainRefrence;

    public SampleRecyclerViewAdapter(Context context,
                                     List<ItemObject> itemList,MainActivity main)
    {
        this.itemList = itemList;
        this.context = context;
        mainRefrence = main;
    }
    public SampleRecyclerViewAdapter(Context context,
                                     List<ItemObject> itemList)
    {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public SampleViewHolders onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.trendy_list, null);
        SampleViewHolders rcv = new SampleViewHolders(layoutView,mainRefrence);
        return rcv;
    }

    @Override
    public void onBindViewHolder(SampleViewHolders holder, int position)
    {
        holder.bookName.setText(itemList.get(position).getName());
        holder.authorName.setText(itemList.get(position).getAuthor());
        holder.img.setImageDrawable(itemList.get(position).getImage());
    }

    @Override
    public int getItemCount()
    {
        return this.itemList.size();
    }
}
