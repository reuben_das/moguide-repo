package com.sample.moguide.santa;

import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.sample.moguide.santa.helper.ItemTouchHelperAdapter;
import com.sample.moguide.santa.helper.ItemTouchHelperViewHolder;


public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {


    private int selected_position = 0;

    private final List<String> mItems = new ArrayList<>();
    static boolean dragging;
    static int fromPos,toPos;
    private static MainActivity mainReference;
    private ArrayList<LatLng> sorted;
    private LinkedList<String> destination;
    private final ArrayList<Integer> icon;
    public interface OnDragStartListener {
        void onDragStarted(RecyclerView.ViewHolder viewHolder);
    }
    private final OnDragStartListener mDragStartListener;
    public RecyclerListAdapter(LinkedList<String> str, ArrayList<LatLng> sort, MainActivity main, OnDragStartListener mDragStartListener,ArrayList<Integer> icons){
        this.mDragStartListener = mDragStartListener;
        mItems.addAll(str);
        destination=str;
        sorted=sort;
        icon=icons;
        mainReference=main;
        System.out.println("mItems in adapterclass: " + mItems);
    }
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_1, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        final int a = position;
        holder.textView.setText(mItems.get(position));
        if (position == 0 || mainReference.mapC.realItinerary.nOfNds == -1) {
            holder.handleView.setImageResource(R.drawable.ic_room_black_48dp);
            holder.textView2.setText("Origin");
            holder.textView4.setText(" ");
        }else {
            holder.textView2.setText(String.format("%.1f", mainReference.mapC.realItinerary.distancebw.get(position - 1)) + " km ");
            holder.textView4.setText(mainReference.mapC.realItinerary.durationArray.get(position - 1));
            switch (icon.get(position))
            {
                case 0:holder.handleView.setImageResource(R.drawable.ic_room_black_48dp);
                    break;

                case 1:holder.handleView.setImageResource(R.drawable.monument);
                    break;

                case 2:holder.handleView.setImageResource(R.drawable.shop2);
                    break;

                case 3:holder.handleView.setImageResource(R.drawable.sight2);
                    break;

                case 4:holder.handleView.setImageResource(R.drawable.amen2);
                    break;

                case 5:holder.handleView.setImageResource(R.drawable.eat2);
                    break;
            }
        }
        int pos = position+1;
        String ch = ""+pos;
        holder.textView3.setText(ch);

        if(selected_position == position){
            holder.itemView.setBackgroundColor(Color.parseColor("#ffffff"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mainReference.check_navigate=1;
                if(mainReference.check_navigate==1){
                    mainReference.mapC.navi(mainReference.check_navigate,mainReference.mapC.retItem(a));
                }
                else if(mainReference.check_navigate==0){
                    Toast.makeText(mainReference.getApplicationContext(),"Select the Navigation button to travel to this destination",Toast.LENGTH_SHORT).show();
                }
                // Updating old as well as new positions
                notifyItemChanged(selected_position);
                selected_position = a;
                notifyItemChanged(selected_position);

                // Do your another stuff for your onClick
            }
        });
    }
    @Override
    public void onItemDismiss(int position) {
        mItems.remove(position);
        mainReference.mapC.removeItem(sorted.get(position));
        notifyItemRemoved(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        String prev = mItems.remove(fromPosition);
        mItems.add(toPosition, prev);
        if (dragging){
            toPos=toPosition;
        }
        else{
            fromPos=fromPosition;
            dragging=true;
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        public final TextView textView;
        public final TextView textView2;
        public final TextView textView3;
        public final TextView textView4;
        public final ImageView handleView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
            textView2 = (TextView) itemView.findViewById(R.id.text2);
            textView3 = (TextView) itemView.findViewById(R.id.text3);
            textView4 = (TextView) itemView.findViewById(R.id.text4);
            handleView = (ImageView) itemView.findViewById(R.id.handle);
        }

        @Override
        public void onItemSelected() {
            //itemView.setBackgroundColor(Color.parseColor("#FFBDBDBD"));
            //mainReference.check_navigate=1;
            //String ch = Long.toString(getItemId());
            //Toast.makeText(mainReference.getApplicationContext(),ch,Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onItemClear() {
            //itemView.setBackgroundColor(Color.parseColor("#FFBDBDBD"));
            if(dragging){
                mainReference.mapC.dragItem(fromPos,toPos);
                dragging=false;
            }
        }
    }
}
