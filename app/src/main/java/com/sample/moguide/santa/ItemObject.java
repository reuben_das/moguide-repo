package com.sample.moguide.santa;

import android.graphics.drawable.Drawable;
import android.media.Image;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Reuben on 06/05/16.
 */
public class ItemObject
{
    private String _name;
    private String _author;
    private Drawable _image;
    private LatLng _location;

    public ItemObject(String name, String auth,Drawable image,LatLng ln)
    {
        this._name = name;
        this._author = auth;
        this._image = image;
        this._location = ln;
    }

    public String getName()
    {
        return _name;
    }

    public Drawable getImage()
    {
        return _image;
    }

    public void setImage(Drawable image)
    {
        this._image = image;
    }

    public LatLng getLocation() {
        return _location;
    }

    public void set_location(LatLng location) {
        this._location = location;
    }

    public void setName(String name)
    {
        this._name = name;
    }

    public String getAuthor()
    {
        return _author;
    }

    public void setAuthor(String auth)
    {
        this._author = auth;
    }
}